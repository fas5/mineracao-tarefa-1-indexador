7 Fast Fixes Makeup Mishaps 
 
Smudged lipstick, streaky foundation, clumpy eyelashes�we've there! we�re head door, catch sight makeup mistake staring mirror. what? Paula�s Choice Research team share moments, we�ve fast fixes common makeup mishaps. don�t start over, tips help ensure you�ll (and looking fantastic) minutes�and share solutions preventing mishap happening future. 
 
Mistake #1: Smudged Lipstick 
 
Fast Fix: slightly damp cotton swab concealer brush bit foundation concealer it, trace where lipstick smudged. issue lipstick traveling lines around mouth, simply trace cotton swab concealer brush around lipline remove excess; reapply lipstick needed. 
 
Stop-it Solution: Unfortunately, thinner, defined lips lines around mouth lipstick gloss easily moves into�issues impossible keep lipstick looking beautiful. anti-feather lipliner line defense issues! helps stop using creamy greasy lipsticks glosses, guaranteed travel lines anyone, (except maybe Angelina Jolie). don't lips like Angelina, don't fret, using matte lipstick matte lip-liner excellent keep lipstick longer minimize smudging. 
 
Mistake #2: Poorly Blended Foundation 
 
Fast Fix: Use slightly damp makeup sponge stipple face soften blend excess foundation. don't sponge, blending clean kabuki foundation brush work. sure blend around jaw hairline, too. you've finished blending, don't forget apply dusting pressed powder smooth out! 
 
Stop-it Solution: don't optimal results foundation, problem actually technique you're using apply foundation. using product learning apply sheer full-coverage foundation, doesn't hurt makeup lesson (available appointment makeup counters) sure you're using technique desired results. Then, experiment home concentrate technique! 
 
technique tried true, problems, issue likely foundation. texture foundation thick thin, won't apply evenly results disappointing�having wrong color makes matters worse! Making sure foundation appropriate skin type important looking best. explore Best Foundations reviews Beautypedia sure you're using well-formulated foundation that's suitable skin type coverage needs. 
 
Remember, takes apply foundation smoothly evenly, you're big hurry, want skip foundation dust pressed powder instead. 
 
Mistake #3: Poorly Blended Eyeshadow 
 
Fast Fix: pressed-powder eyeshadows, wipe excess eyeshadow blend hard edges soft, full eyeshadow brush doesn't product it. that, improve using eyeshadow brush apply lighter shade eyeshadow top area want soften. 
 
Stop-it Solution: Flawless eyeshadow application depends main factors: size eyelid; consider eyelid canvas�the size "canvas" major consideration eyeshadow applied looks you. example, person small, deep-set eyes requires eyeshadow create definition person larger eyelids space eye's crease brow bone. 
 
second consideration tools you're using. question, using brushes gives control creates cleaner, polished look. Plus brush color; don't use brush apply dark brown eyeshadow beige eyeshadow. price eyeshadow on; check list Best Eyeshadows Beautypedia explore selection ones apply beautifully smoothly. 
 
Mistake #4: Over-Tweezed Eyebrows 
 
Fast Fix: Select matte eyeshadow use brow brush fill sparse areas ("holes") eyebrow shape. Brunettes choose shade matches slightly lighter brow color, blondes choose shade slightly darker natural brow color. Applying color dramatically darker lighter actual brow color looks unnatural. 
 
Stop-it Solution: There's denying it's difficult patience reshaping eyebrows, ultimately, best solution filling over-tweezed too-thin eyebrows tweezers. mantra �em grow, �em grow, �em grow. Remove hairs brows, shape back. brows aren't growing, consider prescription Latisse, designed promote eyelash growth, promote eyebrow hair growth. 
 
waiting process, consider enlisting help professional brow bar brow-threading salon assistance. experts able help create brow shape full, symmetrical, flattering face�and help deal awkward grow-out stage. 
 
Mistake #5: Clumpy Mascara 
 
Fast Fix: Apply small amount water or, it's waterproof formula, silicone-based makeup remover spoolie brush clean mascara wand, gingerly wiggle brush across base lashes soften dried mascara separate clumps. try dry mascara spoolie brush, makeup remover water works better smooth big clumps. (The key use teeny-tiny bit don't break mascara want stay lashes). 
 
Stop-it Solution: avoid clumpy messy eyelashes, sure mascara wand minimal amount mascara it. mascaras, mean gently wiping brush tissue applying. Next, apply mascara starting base lashes, slightly wiggling brush forth move ends lashes. create volume clumping, apply additional layers using short upward strokes�and coat dries. 
 
mascara clumping flaking, issue likely mascara�not application technique! Don't worry, getting mascara doesn't require breaking bank department store brands; excellent drugstore options well. Check list Best Mascaras Beautypedia several options perform beautifully $10! 
 
Mistake #6: Concealer Creasing Lines Around Eyes 
 
Fast Fix: Wash hands, use ring finger smooth concealer begun crease. Prevent concealer creasing setting small amount pressed powder. best results, use small brush sponge, making sure blend powder where eye nose meet, softening temple where crow's-feet lines reach; want avoid putting excess powder wrinkles. 
 
Stop-it Solution: sure prep eye area thin layer moisturizer (it need eye cream); apply sparingly overdoing cause creasing. Choose concealer appropriate skin type, smooth, easy blend, long-wearing, opaque enough cover undereye darkness. thinner texture, likely experience creasing. Check list Best Concealers reliable options. set under-eye concealer powder (ideally, powder sunscreen!). 
 
Mistake #7: Poorly Blended Blush 
 
Fast Fix: accidentally apply blush, impulse blend color entire cheek area (in attempt sheer color). Don't it! You'll strange rosy patches reaching jawline. Instead, use clean powder brush (preferably, densely packed hair) pick blend excess product rubbing brush small circular motions apples cheeks temples. Double-check sure hard edges�if are, blend using slightly dampened makeup sponge. 
 
Stop-it Solution: Blush seem like beauty basic, it's challenges. sure you're applying blush full-size blush brush. brushes included compacts small provide proper application, guarantee you'll stripe color, women we're out. blush brushes longer, looser hair pick color apply evenly. important want start sheer amount color, apply necessary. makeup, it's easier add subtract! 
 
applying blush, start using fullest brush pick color tap excess. Smile (to rounded apples cheeks), relax expression use upward, controlled swirling motion apply blush apples toward temples. 
