Beauti to die for: health hazard of cosmet and skin care product reveal 
 
Your medicin cabinet i on of the most danger area of your house, and not for the reason you mai think. Lurk just behind your bathroom mirror, where all of your favorit beauti product ar housed, i a virtual toxic nightmare. The grow list of synthet ingredi manufactur add to their product i turn the most innocent-look shampoo and moistur into cocktail of toxin that could caus cancer or reproduct damag over year of sustain use. Modern cosmet contain a host of danger ingredients, which would be more at home in a test tube than in our bodies. 
Like most people, you probabl assum that the ingredi found in beauti product have been thoroughli test for safeti well befor thei land on your groceri store' shelves. After all, the govern ha regul in place for the water we drink, the food we eat and the air we breathe. One would assum that the FDA would also be overse the cosmet industri to ensur the health and safeti of consumers. Unfortunately, the FDA ha littl power when it come to regul the ingredi found in your beauti products. In fact, the onli peopl ensur the safeti of person care product ar the veri peopl who govern the industry: The Cosmet Toiletri and Fragranc Associat (CTFA). Scientist paid by the CTFA make up the Cosmet Ingredient Review panel (CIR) and ar charg with regul the safeti of the industry' products. 
 
In 2004, the Environment Work Group (EWG) releas the find of a studi it conduct regard the safeti of beauti care products. Compar approxim 10,000 ingredi found in 7,500 differ product against list of known and suspect chemic health hazards, the research reveal that the CIR wa fall tragic short of ensur consum safety. 
 
Of the 7,500 product test by the EWG, a mere 28 had been evalu for safeti by the CIR. The EWG found that on in everi 120 product analyz contain ingredi certifi by the govern a known or probabl carcinogen and that nearli one-third of the product contain ingredi classifi a possibl carcinogens. Astoundingly, 54 product even violat recommend for safe us that the CIR had put in place, yet these product ar still avail for sale today. 
 
Of the product tested, the worst offend were those contain the cancer-caus ingredi coal tar, alpha hydroxi acid and beta hydroxi acids, and those contain the hormone-disrupt ingredient, phthalate. 
 
Coal Tar 
	 
Seventy-on hair dye product evalu were found to contain ingredi deriv from coal tar (list a FD&C or D&C on ingredi labels). Sever studi have link long-tim hair dye us to bladder cancer, non-Hodgkin� lymphoma and multipl myeloma. 
A research studi conduct in 2001 by the USC School of Medicin found that women us perman hair dye at least onc a month more than doubl their risk of bladder cancer. The studi estim that "19 percent of bladder cancer in women in Lo Angeles, California, mai be attribut to perman hair dye use." 
 
A link between hair dye and non-Hodgkin' lymphoma wa establish in 1992 when a studi conduct by the Nation Cancer Institut found that 20 percent of all case of non-Hodgkin� lymphoma mai be link to hair dye use. 
 
While the FDA ha not step in to prevent the us of coal tar in beauti products, it doe advis consum that reduc hair dye us will possibli reduc the risk of cancer. 
 
Alpha Hydroxi Acid (AHA) & Beta Hydroxi Acid (BHA) 
 
Alpha Hydroxi Acid and Beta Hydroxi Acid ar commonli us in product advertis to remov wrinkles, blemishes, blotch and acn scars. With consum complaint of burning, swell and pain associ with AHA and BHA flood into the FDA, the regulatori bodi began conduct it own research about 15 year ago. The find link the us of AHA and BHA with a doubl of UV-induc skin damag and a potenti increas risk of skin cancer. 
Accord to the Environment Protect Agency, skin cancer ha reach "epidem proportions," with 1 million new case occur each year and on person dy everi hour from the disease. The agenc estim that, at the current rate, on in five peopl will develop skin cancer over their lifetime. 
 
The FDA' studi find were present to the CIR, but the panel approv the continu us of AHA and BHA "in spite of seriou safeti question submit by a consum group and a major manufacturer," accord to an FDA spokesperson. 
 
Even though on out of everi 17 product analyz by the EWG studi contain either AHA or BHA (with nearli 10 percent be moistur and 6 percent sunscreens), the most that the FDA could do wa suggest that product contain the ingredi carri a warn to us sunscreen and to limit sun exposur while us the product. A puzzl solution, sinc some of the product contain the danger ingredi ar design specif for us in the sun. 
 
Phthalat 
 
Phthalat ar industri plastic wide us in person care product to moistur and soften skin, impart flexibl to nail polish after it dri and enhanc the fragranc us in most products. Studi indic that phthalat caus a wide rang of birth defect and lifelong reproduct impairments, target everi organ in the male reproduct system and caus problem rang from low sperm count to seriou genit deform that can lead to an increas risk of cancer. 
While the EWG onli found four product with phthalat list a an ingredi (all nail care products), there i no tell how mani product actual contain it. The industri i not requir to list fragranc ingredi or "trade secret" ingredi on products, and phthalat often fall into on of those two categories. 
 
In Septemb 2004, the European Union implement a ban on all beauti product contain phthalates. California Assemblywoman Judi Chu ha propos a similar bill (AB 908) to be vote on later thi year that would implement the same ban in the Unite States. Opponent of the bill, mainli the CTFA, argu that chang label process would present a huge econom burden and could infring on trade secrets. A similar bill fail just last year. 
 
Four Step of Action 
 
1. Go to www.ewg.org and check out the health risk of your favorit products. EWG ha compil a guid of 7,500 beauti care product and ha rank them accord to their ingredients' potenti to caus cancer, trigger allerg reactions, interfer with the endocrin (hormonal) system, impair reproduct or damag a develop fetus. 
2. Visit the FDA' websit at www.fda.gov and familiar yourself with the step that you can take in order to file complaint or concern about consum products. 
 
3. Visit www.safecosmetics.org to learn more about how you can becom involv with bill AB 908 to ban phthalat in beauti product in the Unite States. 
 
4. Check out my recommend for all-natur and safe product for both you and your famili at www.scmedicalcenter.com. All product mention have been us safe and with wonder result by my patient for years. 
 
Dr. Connealy, M.D., M.P.H., began privat practic in 1986. In 1992, she found South Coast Medic Center for New Medicin where she serv a medic director. Her practic i firmli base in the belief that strictli treat health problem with medic doe not find the root caus of the illness. Dr. Conneali write monthli column for Coast and OC Health magazines, and i a bi-weekli guest on Frank Jordan' "Healthy" radio show. She routin lectur and educ the public on health issues. 
