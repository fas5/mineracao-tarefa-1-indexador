From: mserv@mozart.cc.iup.edu (Mail Server) 
Subject: Christian Homosexu (part 1 of 2) 
 
Note:  I am break thi repli into 2 part due to length. 
 
mls@panix.com (Michael Siemon) writes: 
>In articl <May.11.02.36.34.1993.28074@athos.rutgers.edu> 
>mserv@mozart.cc.iup.edu (someon name Mark) writes: 
>>mls@panix.com (Michael Siemon) writes: 
>>>Homosexu Christian have inde "check out" these verses.  Some of 
>>>them ar us against u onli through incredibli pervers interpretations. 
>>>Other simpli do not address the issues. 
>> 
>>I can see that some of the abov vers do not clearli address the issues, 
> 
>There ar exactli ZERO vers that "clearly" address the issues. 
 
I agre that there ar no vers that have gone unchalleng by gai right 
activists.  But if there ar zero vers that "'clearly' address the issues," 
doesn't that mean that there ar also no vers that clearli *support* your 
case?  Are you sure you want to sai that there ar zero vers that clearli 
address the issues? 
 
>>however, a coupl of them seem a though thei do not requir "incredibli 
>>pervers interpretations" in order to be seen a condemn homosexuality. 
> 
>The kind of interpret I see a "incredibli perverse" i that appli 
>to the stori of Sodom a if it were a blanket equat of homosexu 
>behavior and rape.  Sinc Christian cite the Bibl in such a context 
>should be presum to have at least READ the story, it amount to slander 
>-- a charg that homosexu == rape -- to us that against us. 
 
The stori in Genesi 19 tell of the citizen of Sodom demand an opportun 
to "know" the two men who were Lot' guests; the fact that the Sodomit becam 
angri when Lot offer them hi daughter could be seen a indic that thei 
were interest onli in homosexu intercourse.  Yes, what thei want wa 
rape, homosexu rape, and everybodi agre that that i wrong.  Some 
Christian believ that the homosexu aspect of their desir wa just a 
sin a the rape aspect of their desire.  The passag doe not sai what it 
wa that so offend God, whether it wa the homosexuality, or the intend 
rape, or both, but I believ that it i onli fair to consid all the possibl 
altern in the light of relat Scriptures.  I do not believ that those 
who believ God wa offend by both the homosexu and the rape ar try 
to sai that homosexu i itself a form of rape. 
 
You seem to take the view that the *only* sin describ in Gen. 19 i in the 
fact that the Sodomit want to commit rape, and that it i unfair to 
"stigmatize" their homosexu by associ it with the sin of rape.  I can 
see how you might reach such a conclus if you start from the conclus 
that there i noth wrong with homosexuality, but then again we'r not 
suppos to start from our conclus becaus that' circular reasoning.  If 
God i in fact oppos to homosexu intercours in general, then the more 
probabl interpret i that He wa at least a offend by the Sodomites' 
blatant homosexu a He wa by their intent to commit rape.  Later on I 
will document why I believ the Old Testament portrai God a One who despis 
*any* homosexu intercourse, even if both partner ar consent adults. 
 
>>"... Do not be deceived; neither fornicators, nor idolators, nor adulterers, 
>>nor effeminate, nor homosexuals, nor thieves, nor the covetous, nor 
>drunkards, 
>>nor revilers, nor swindlers, shall inherit the kingdom  of God.  And such 
>were 
>>some of you..."  I Cor. 6:9-11. 
> 
>The moder adequ discuss the circular of your us of _porneia_ 
>in this. 
 
The moder found my propos to be circular in that he regard the church 
a the proper  author for determin what *kinds* of marriag would be 
legitimate, and thu the church' refus to recogn "perverted" marriag 
wa circular reasoning.  My questions, however, had noth to do with the 
church ordain new kind of marriages, and so hi argument wa someth of a 
straw man.  In term of my origin question, the precis 
definition/transl of "porneia" isn't realli important, unless you ar 
try to argu that the Bibl doesn't realli condemn extramarit sex.  I'm 
not sure the moder wa try to do that. 
 
In ani case, I think both you and the moder have miss the point here. 
When Jesu wa ask about divorce, He replied, "Have you not read, that He who 
creat them from the begin made them male and female, and said, 'For thi 
caus a man shall leav hi father and mother and shall cleav to hi wife; and 
the two shall becom on flesh'?  Consequ thei ar no longer two, but on 
flesh.  What therefor God ha join together, let no man separate." (Mt. 
19:4-6).  I read here that the sexual union of a man (male) and hi wife 
(female) i a divinely-ordain union.  In other words, the institut of 
heterosexu marriag i someth ordain and establish by God--not by men, 
and not by the church, but by God.  Men ar not suppos to dissolv thi 
union, in Jesus' words, becaus it i not someth creat by men. 
 
Thi i not circular reasoning, thi i just read God' word.  I read in the 
Bibl that God ordain the union of male and female.  I do not read of ani 
similar divinely-ordain union of two male or two females.  Granted, there 
have been uninspir men who have ordain "alternative" union (isn't Caligula 
report to have "married" hi horse?), but the onli union that Jesu refer to 
a "what God ha join together" i the heterosexu union of a man and hi 
wife. 
 
(Pardon me for mention Caligula.  I know that' probabl inflammatory, and I 
should save it for the discuss on bestiality, in part 2 of thi post. 
Pleas hold off on pass judgement on me until you have read that section of 
my reply.) 
 
Anyway, my origin question wa not whether we should translat "porneia" in a 
wai that condemn onli a select few kind of extramarit sex, my question was: 
given that heterosexu marriag i the onli union describ by the Bibl a 
divinely-ordained, and given a Biblic prohibit against sex outsid of 
marriage, i homosexu intercours sinful?  Of course, I see now that first we 
need to ask whether the Bibl realli condemn sex outsid of marriage.  You 
seem to be try to argu that onli certain kind of extramarit sex (and 
other sins) ar realli wrong: 
 
>>I think we can all agre (with Paul) that there ar SOME kind of 
>activ that could be name by "fornication" or "theft" or "coveting" or 
>"reviling" or "drunkenness" which would well deserv condemnation.  We mai 
>or mai not agre to the bound of those categories, however; and the veri 
>fact that thei ar argu over suggest that not onli i the matter not at 
>all "clear" but that Paul -- an excel rhetorician -- had no interest 
>in MAKING them clear, leav matter rather to our Spirit-l decisions, 
>with all the uncomfort living-with-other-read that ha domin 
>Christian discuss of ALL these areas. 
 
Alternatively, it mai be that the definit of such term a "porneia" and all 
the rest was, in Paul' day, what we would call a FAQ; i.e. the Law, a the 
"tutor" appoint by God to lead u to Christ, had just spent some sixteen 
centuri drum into the head of God' peopl the idea that thing like 
homosexu intercours were abomin that deserv punish by death. 
Perhap Paul didn't go into detail on what "porneia" &c were becaus after 1600 
year he consid the question to have been dealt with already.  Perhap the 
reason God' apostl and prophet did not devot a great deal of time defin 
a distinct, New Testament sexual wa becaus He did not intend ani 
signific chang in the sexual He had alreadi establish by the Law. 
I'll discuss the Law and homosexu in greater detail below, but I just 
want to point out that the New Testament' failur to develop a detail new 
standard of sexual i not necessarili evid that God doe not care about 
sexual conduct--especi after 1600 year of put peopl to death for 
practic homosexuality! 
 
>Homosexu behavior i no different.  I (and the other gai Christian I 
>know) ar adam in condemn rape -- heterosexu or homosexu -- and 
>child molest -- heterosexu or homosexu -- and even the possibli 
>"harmless" but obsess kind of sex -- heterosexu or homosexu -- 
>that would stand condemn by Paul in the veri continu of the chapter 
>you cite [mai I mildli suggest that what *Paul* doe in hi letter that 
>you want to us i perhap a good guid to hi meaning?] 
> 
>        "'I am free to do anything,' you say.  Yes, but not everyth 
>        i for my good.  No doubt I am free to do anything, but I for on 
>        will not let anyth make free with me."  [1 Cor. 6:12] 
> 
>Which i a restat that we must have no other "god" befor God.  A 
>command neither I nor ani other gai Christian wish to break.  Some 
>peopl ar inde involv in obsess driven mode of sexual behavior. 
>It i just a wrong (though slightli less incendiary, so it' a secondari 
>argument from the 'phobic contingent) to equat homosexu with such 
>behavior a to equat it with the rape of God' messengers. 
 
And how do you defin an "obsess driven" mode of sexual behavior?  How do 
you determin the differ between obsess sexual behavior and normal sex 
drives?  I the desir to have "sinful" sex an obsess driven mode of 
behavior?  I think you see that thi i circular reasoning:  Why i it defin 
a sinful?  Becaus it i obsessive.  What make it obsessive?  The fact that 
the person i driven to seek it even though it' sinful.  Or i it obsess 
becaus it i a desir for that which societi condemns?  Once again, that' 
circular:  Why i it defin a obsessive?  Becaus the person want it even 
though societi condemn it.  Why doe societi condemn it?  Becaus it i 
obsessive. 
 
You seem to be try to limit the Bible' condemn of "porneia" to onli 
"perverted" sex acts, but I don't think you can realli defin "perverted" 
without fall into exactli the same circular you accus me of.  What, 
then, i Paul condemn when he declar that "Fornicators...shal not enter 
the kindgom of heaven"? 
 
>I won't deal with the exegesi of Leviticus, except veri tangentially. 
>Fundamentally, you ar exhibit the same circular here a in your 
>assumpt that you know what _porneia_ means. 
 
I think you misunderstood me:  I wa not try to make an argument on some 
technic definit of "porneia", I wa rais the issu of the sin 
of extramarit sex and the lack of ani Scriptur evid of a homosexu 
counterpart to the divinely-ordain union of heterosexu couples. 
 
>There ar plenti of 
>law prohibit sexual behavior to be found in Leviticus, most of 
>which Christian ignor completely.  Thei never even BOTHER to examin 
>them.  Thei just *assume* that thei know which on ar "moral" and 
>which on ar "ritual."  Well, I have new for you.  Any anthropologi 
>cours should sensit you to ritual and clean vs. unlcean a categori 
>in an aw lot of societi (we have them too, but buri pretti deep). 
>And I cannot see ani ground for distinguish these bit of Leviticu 
>from the "ritual law" which NO Christian I know feel appli to us. 
> 
>I'm dead seriou here.  When peopl start go on (a thei do in thi 
>matter) about how "repulsive" and "unnatural" our act ar -- and what 
>do thei know about it, huh? -- it i a solid clue to the same sort of 
>arbitrari cultur inculc a the American prejudic against eat 
>insects. 
 
Pleas rememb what you just said here for when we discuss bestiality, in part 
2. 
 
>On what basis, other than assum your conclusion, can you 
>sai that the law against male-mal intercours in Leviticu i NOT a part 
>of the ritual law? 
 
I am glad you asked.  Would you agre that if God condemn homosexu 
intercours even among those who ar not under the Law of Moses, then thi 
would show that God' condemn of homosexu act goe beyond the ritual 
law?  If I can show you from Scriptur that God punish the homosexu 
behavior of peopl who were *not* under the Law of Moses, would you agre that 
God' definit of homosexu intercours a an abomin i not limit to 
just the ritual law and those who ar under the Law? 
 
I've been have a privat Email discuss with a 7th Dai Adventist on the 
subject of the Sabbath, and my main point against a Christian sabbath-keep 
requir ha been that nowher in Scriptur doe God command Gentil to 
rest on the sabbath, nor doe He ever condemn Gentil for fail to rest on 
the Sabbath.  Thi illustr the differ between univers requir 
such a "Thou shalt not kill", and requir that ar mere part of the 
(temporary, Jews-only) Law of Moses, such a the Sabbath. 
 
The point you ar try to make i that you think the classif of 
homosexu intercours a "an abomination" i *just* a part of the temporary, 
Jews-onli Law of Moses.  I on the other hand believ that it wa label by 
God a an abomin for Gentil a well a Jews, and that He punish those 
guilti of thi behavior by death or exile.  Here' why: 
 
Back in Genesi 15, God promis to give Abraham all the land that wa then in 
the possess of "the Amorite"--kinda hard on the Amorite, don't you think? 
But in vers 16 we have a clue that thi might not be a unjust a it sounds: 
it seem God i go to postpon thi takeov for quit a while, becaus "the 
iniqu of the Amorit i not yet complete". 
 
Remember, thi i all long befor there wa a ritual law.  What then wa the 
iniqu the Amorit wa commit that, when complete, would justifi hi 
be cast out of hi own land and/or killed?  Go back and look at Lev. 18 
again.  Vers 1-23 list a varieti of sins, includ child sacrifice, incest, 
homosexuality, and bestiality.  Begin in vers 24, God start saying, "Do 
not defil yourselv by ani of these things; for _by_all_these_ _things_ the 
nation which I am cast out befor you _have_ _become_defiled_.  For the 
land ha becom defiled, therefor I have visit it punish upon it, so 
the land ha spew out it inhabitants... For whoever doe ani of these 
abominations, those person who do so shall be cut off from among their 
people." 
 
Notic that God sai the Gentil nation (who ar *not* under the ritual Law of 
Moses) ar about to be punish becaus thei have "defiled" themselv and 
their land by commit "abominations" that includ incest, bestiality, and 
homosexuality.  Flip ahead two chapter to Lev. 20, and you will find these 
same "abominations" listed, and thi time God decre the death penalti on 
anyon involv in ani of these things, including, specifically, a man "ly 
with anoth man a on li with a woman" (Lv. 20:13).  Their 
"bloodguiltiness" wa upon them, mean that in God' eyes, thei deserv to 
die for have done such things.  Accord to Lev. 18:26-29, even "the alien 
[non-Jew] who sojourn among you" wa to refrain from these practices, on 
penalti of be "cut off [by God?] from among their people." 
 
Under the circumstances, I believ it would be veri difficult to support the 
claim that in the Old Testament God object onli to the intend rape, and not 
the homosexuality, in Sodom.  Sinc God took the troubl to specif list 
sex between two consent men a on of the reason for wipe out the 
Canaanit nations, (not homosexu rape, mind you, but plain, voluntari gai 
sex), I'd sai God wa not neutral on the subject of homosexu behavior, even 
by those who had noth to do with the Mosaic Covenant. 
 
>For those Christian who *do* think that *some* part of Leviticu can 
>be "law" for Christian (while other ar not even to be thought about) 
>it i incumb on you *in everi case, handl on it own merits* to 
>determin why you "pick" on and ignor another. 
 
Accord to II Tim. 3:16, all Scriptur i inspir by God and i profit 
for teaching, reproof, correction, and train in righteousness; thus, I 
believ that even though we Gentil Christian ar not under the Law, we can 
learn from studi it.  If a certain action i defin a a sin becaus it i 
a violat of the Law, then it i a sin onli for those who ar under the Law 
(for example, in the case of Sabbath-keeping).  Where God reveal that certain 
action ar abomin even for those who ar not under the Law, then I 
conclud that God' object to the practic i not base on whether or not a 
person i under the Law, but on the sin of the act itself.  In the case 
of homosexuality, homosexu intercours i defin by God a a defil 
abomin for Gentil a well a Jews, i.e. for those who ar not under the 
Law a well a for those who are.  Thus, I am not at all try to sai that 
Gentil Christian have ani oblig to keep ani part of the Law, I am simpli 
sai that God refer to homosexu a a sin even for those who ar not 
oblig to keep the Law.  If thi i so, then I do not think we can appeal to 
our exempt from the Law a valid ground for legitim a practic God ha 
declar a bloodguilti abomin that defil both Jew and Gentile. 
 
(continu in Part 2) 
 
- Mark 
