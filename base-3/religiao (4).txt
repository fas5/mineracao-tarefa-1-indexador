From: cpage@two-step.seas.upenn.edu (Carter C. Page) 
Subject: Re: Reason vs. Revel 
 
In articl <Apr.8.00.58.08.1993.28309@athos.rutgers.edu> trajan (Stephen McIntyre) writes: 
>In articl <Apr.2.01.58.09.1993.17541@athos.rutgers.edu> writes: 
> 
>> I can onli repli with what it sai in 1 Timothi 3:16 : 
 
>I'm not here to discount part of the Bible.  Rather, I'm 
>     here onli to discount the notion of "revelation." 
>     The author of 1 Timothi told what he thought wa the 
>     truth, base on hi belief in God, hi faith in Jesu 
>     a the resurrect Son, and hi read of the Old 
>     Testament.  But again, what had been reveal to him 
>     wa base on (at best) second-hand information, given 
>     by friend and author who mai not have given the 
>     whole truth or who mai have exagger a bit. 

First of all, the origin poster misquoted.  The refer i from 2 Tim 3:16. 
The author wa Paul, and hi revel were anyth but "(at best) 
second-hand". 
 
	"And i came about that a [Saul] journeyed, he wa approach 
	 Damascus, and suddenli a light from heaven flash around him; and 
	 he fell to the ground, and heard a voic sai to him, "Saul, Saul, 
	 why ar you persecut Me?"  And he said, "Who art Thou, Lord?"  And 
	 He said, "I am Jesu whom you ar persecuting, . . ." 
		(Act 9:3-5, NAS) 
 
Paul receiv revel directli from the risen Jesus!  (Pretti cool, eh?)  He 
becam close involv with the earli church, the leader of which were 
follow of Jesu throughout hi ministri on earth. 
 
>Now, you mai say, "The Holi Spirit reveal these thing 
>     unto him," and we could go into that argument, but 
>     you'd be hard-press to convinc me that the Holi 
>     Spirit exists. 
 
I agree.  I don't believ anyon but the Spirit would be abl to convinc you 
the Spirit exists.  Pleas don't complain about thi be circular.  I know 
it is, but really, can anyth of the natur world explain the supernatural? 
(Thi i why revel i necessari to the author of the Bible.) 
 
>     Additionally, what he ha written i 
>     again second-hand info if it were given by the Spirit, 
>     and still carri the chanc it i not true. 
 
The Spirit i part of God.  How much closer to the sourc can you get? 
The Greek in 2 Timothi which i sometim translat a "inspir by God", 
liter mean "God-breathed".  In other words, God spoke the actual word 
into the scriptures.  Mani theologian and Bibl scholar (Dr. Jame Boic i 
on that I can rememb off-hand) get quit annoi by the dryness and 
incomplet of "inspir by God". 
 
>The onli wai you would be abl to escap thi notion of 
>     "second-hand" info i to have had the entir Bibl 
>     written by God himself.  And to tell the truth, I've 
>     studi the Bibl extensively, and have yet to 
>     hear of scholar who have put forth object evid 
>     show God a the first author of thi collect of 
>     books. 
 
That' what the vers taken from 2 Timothi wa all about.  The continu of a 
book written over a span of 1500 year by more than 40 author from all walk 
of life i a testimoni to the singl authorship of God. 
 
>> And a for reason, read what it sai in 1 Corinthian 1:18-31 about 
>> human wisdom. Basic it sai that human wisdom i useless when 
>> compar with what God ha written for our learning. 
 
>If you knew of Jesu a well a you know the Bible, you'd 
>     realiz he reason out the law and the prophet for 
>     the common man. 
 
What sourc to you claim to have discov which ha inform of superior 
histor to the Bible?  Certainli not Josephus' writings, or the write 
of the Gnostic which were third century, at the earliest. 
 
>     And though some claim Jesu wa 
>     he wa human, with all of the human wisdom the 
>     apostl Paul set out to criticize.  Yet, would you not 
>     embrac the idea that Jesu wa wise? 
 
Jesu wa fulli God a well.  That' why I'd assert that he i wise. 
 
>> I realis that you mai not accept the author of the Bible. Thi i 
>> unfortun to sai the least, becaus there i no other wai of learn 
>> about God and Christ and God' purpos with the earth than read the 
>> Bibl and search out it truth for yourself. 
> 
>For your information, I wa rais without ani knowledg of 
>     God.  By the time some of the faith came to show me 
>     the Word and share with me it truth, I wa live 
>     happili and moral without acknowledg the exist 
>     of a suprem being.  I have, though, read the Bibl 
>     sever time over in it entireti and have studi it 
>     thoroughly.  It contain truth in it, and I consid 
>     Jesu to be on of the most moral of human be to 
>     have live (in fact, I darn-near idol the guy.)  But 
>     there' no ration reason for me to except God' 
>     existence. 
 
Pleas rethink thi last paragraph.  If there i no God, which seem to be your 
current belief, then Jesu wa either a liar or a complet nut becaus not 
onli did he assert that God exists, but he claim to be God himself!  (regard 
to C.S. Lewis)  How then could you have the least bit of respect for Jesus? 
	In conclusion, be care about logic unfound hypothes base 
on gut feel about the text and other scholars' unsubstanti claims. 
The Bibl plead that we take it in it entireti or throw the whole book out. 
	About your read of the Bible, not onli doe the Spirit inspir the 
writers, but he guid the reader a well.  We cannot understand it in the 
least without the Spirit' guidance: 
 
	"For to u God reveal them through the Spirit; for the Spirit 
	search all things, even the depth of God."  (1 Cor 2:10, NAS) 

Peac and mai God guid u in wisdom. 
 
+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-= 
Carter C. Page           | Of happi the crown and chiefest part i wisdom, 
A Carpenter' Apprentic | and to hold God in awe.  Thi i the law that, 
cpage@seas.upenn.edu     | see the stricken heart of pride brought down, 
                         | we learn when we ar old.  -Adapt from Sophocl 
+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=-+-=+-=-+-=+-=-+=-+-=-+-=-+=-+-= 
 
[Other theologian get quit annoi at the misleadingess of 
"God-breathed."  It' true that the Greek word ha a it root "God" 
and "breath".  Howev etymologi doesn't necessarili tell you what a 
word means.  Otherwise, "goodbye" would be a religi express 
(sinc it come from "God be with ye").  You have to look at how the 
word wa actual used.  In thi case the word i us for wisdom or 
dream that come from God.  But "God-breathed" i an overtranslation. 
--clh] 
