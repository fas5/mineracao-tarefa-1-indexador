From: trajan@cwis.unomaha.edu (Stephen McIntyre) 
Subject: Re: The Problem of Satan (use to be: islam author over women) 
 
In articl <1993Apr5.165233.1007@news.unomaha.edu> trajan@cwis.unomaha.edu 
     (Stephen McIntyre) writes: 
 
> Of course, Bobbi then state that Satan ha no free will, that 
>    he doe a God want him to.  Thi bring up a host of 
>    paradoxes:  i God therefor evil; do I have free will 
>    or i God direct me also; if God i evil, which part 
>    of hi infinit self i good and which i evil; etc.? 
 
> I would like for onc a solid answer, not a run-about. 
 
# I hope I gave you a fairli solid answer to thi one: I simpli don't agre 
# with the embodi version of a Satan who i a separ creation or a force. 
# I wrote: 
 
>> The belief to which I ascrib i that evil i not a creation; 
>> rather, it i "the absenc of good."  Thi fit with all the 
>> logic about thing have dual use: e.g., a knife can be us 
>> to sculpt and it can be us to kill.  Like entropy, evil i 
>> seen in thi view a neither forc nor entity.  Satan is, 
>> therefore, metaphorical.  In fact, there ar sever vers 
>> of the Holi Qur'an which appear to support thi view and sever 
>> Tradit a well. 
> 
>> For example, there i a Tradit that food should never be left open 
>> on a shelf or tabl overnight, lest "Satan" enter it.  It appear 
>> that thi i a refer to a yet undiscov germs; thus, the 
>> evil effect of spoil food i describ a "Satan." 
 
>But there ar mani exampl of Satan personified.  Which am I 
>     to believe? 
 
# And there ar quit physic descript of Heaven and Hell in the 
# Holi Qur'an, the Bible, etc.  There have been time in the spiritu 
# and intellectu evolut of the modern human when these physic 
# descript of Heaven, Hell, and Satan were taken quit liter 
# and that *worked* for the time.  A I mention in the Tradit 
# cite above, for example, it wa suffici in the absenc of a theori 
# about germ and diseas spread by worm to simpli describ the "evil" 
# which wa pass to a consum of spoil food a "satanic." 
 
     Which beg the question: if Satan in thi case i 
     metaphorical, how can you be certain Allah i not 
     the same way? 
 
# The bottom line here, however, i that describ a spiritu plane 
# in human languag i someth like describ "color" to a person 
# who ha been blind from birth.  You mai want to read the book 
# FLATLAND (if you haven't already) or THE DRAGON'S EGG.  The first 
# i intend a a light heart descript of a mathemat con- 
# cept... 
 
[some delet for space saving] 
 
# When languag fail becaus it cannot be us to adequ describ 
# anoth dimens which cannot be experienc by the speakers, then 
# such convent a metaphor, allegory, and the like come to be 
# necessary.  The "unseen" i describ in term which have reference` 
# and mean for the reader/listener.  But, like all models, a compro- 
# mise must be made when speak metaphorically: clariti and direct 
# of meaning, equival of perception, and the like ar all 
# crippled.  But what els can you do? 
 
     Thi i why I ask the above.  How would you then 
     know God exist a a spirit or be rather than 
     just be metaphorical?  I mean, it' okai to sai 
     "well, Satan i just metaphorical," but then you 
     have to justifi thi belief AND justifi that God i 
     not some metaphor for someth else. 
 
     I sai thi becaus there ar many, mani instanc of 
     Satan describ a a be (such a the tormentor in 
     the Old Testament book of Job, or the temptor in the 
     New Testament Gospels).  In the same way, God too i 
     describ a a be (or spirit.)  How am I to know 
     on i metaphor and not the other. 
 
     Further, belief in God isn't a bar to evil.  Let' 
     consid the case of Satanists: even if Satan were 
     metaphorical, the Satanist would have to believ 
     in God to justifi thi belief.  Again, we have a 
     case where someon doe believ in God, but by 
     religi standards, thei ar "evil."  If Bobbi 
     doe see this, let him address thi question also. 
 
[delet some more on "metaphor"] 
 
>> Obviousli more philosoph on thi issu i possible, but I'm 
>> not sure that the reader of thi newsgroup would want to delv 
>> into religi interpret further.  However, if anyon wish 
>> to discuss this, I'm certainli will (either off line - e-mail - or 
>> on line - posting). 
 
Stephen 
 
    _/_/_/_/  _/_/_/_/   _/       _/    * Atheist 
   _/        _/    _/   _/ _/ _/ _/     * Libertarian 
  _/_/_/_/  _/_/_/_/   _/   _/  _/      * Pro-individu 
       _/  _/     _/  _/       _/       * Pro-respons 
_/_/_/_/  _/      _/ _/       _/ Jr.    * and all that jazz... 
 
 
-- 
