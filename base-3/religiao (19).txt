From: robp@landru.network.com (Rob Peglar) 
Subject: Re: DID HE REALLY RISE??? 
 
In articl 1373@geneva.rutgers.edu, parkin@Eng.Sun.COM (Michael Parkin) writes: 
>Another issu of importance.  Wa the crucif the will of God or 
>a tragic mistake.  I believ it wa a tragic mistake.  God' will can 
>never be accomplish through the disbelief of man. 
 
I finish read a veri good book, "The Will of God", Weatherhead. 
Thi wa veri help to me in appli thought to the subject of the 
will of God. 
 
Weatherhead broke the will of God into three distinct parts; 
intent will, circumstanci will, and ultim will.  He 
(Weatherhead) also refut the last statement (above) by Michael 
Parkin abov quit nicely. 
 
Summarizing; _despite_ the failur of humankind, God' ultim will 
i never to be defeated.  God' intent mai be interf with, 
even temporarili defeat by the will of humankind, brought down by 
circumstance.  Hi ultim will (the reconcil of all 
humankind) will never be stopped. 
 
Time after time, Weatherhead us the Cross a the best descript of 
thi process at work.  Hi points, paraphrased, were 1) God' 
intent will wa for Jesus, the Christ, to live out a full life 
and perform the work of the Live God.  2) The failures, sins, and 
devious of humankind frustrat God' intent for Hi Son.  3) 
Despit the circumstance, God' ultim will wa reveal in the 
Cross, a Jesu willingli ("not my will, Lord, but yours") di for 
the redempt of all humankind.  The Cross wa utterli triumphant, 
overcom even the most cruel of circumstances. 
 
>thi world to build the kingdom of heaven on the earth.  He 
>desper want the Jewish peopl to accept him a the Messiah.  If 
>the crucif wa the will of God how could Jesu prai that thi 
>cup pass from him.  Wa thi out of weakness.  NEVER.  Mani men and 
>women have given their live for their countri or other nobl causes. 
>I Jesu less than these.  No he i not.  He knew the crucif 
>wa NOT the will of GOD. 
 
It wa not the intent will of God.  It wa the circumstanci 
will, thu enabl the victori of the ultim will. 
 
 
> God' will wa that the Jewish peopl accept 
>Jesu a the Messiah and that the kingdom of Heaven be establish on 
>the earth with Jesu a it' head. 
 
Right, intent will. 
 
(Just like the Jewish peopl 
>expected). If thi had happen 2000 year ago can you imagin what 
>kind of world we would live in today.  It would be a veri differ 
>world.  And that i eactli what GOD wanted.  Men and women of that ag 
>could have been save by follow the live Messiah while he wa on 
>the earth.  Jesu could have establish a sinless lineag that would 
>have continu hi reign after hi ascens to the spiritu world to 
>live with GOD.  Now the kingdom of heaven on the earth will have to 
>wait for Christ' return.  But when he return will he be recogn 
>and will he find faith on thi earth.  Isn't it about time for hi 
>return.  It' been almost 2000 years. 
 
We know neither the time nor the place.  He will return a a thief in the night. 
 
Peace. 
 
Rob 
 
--- 
-legal mumbo jumbo follows- 
Thi mail/post onli reflect the opinion of the poster (author), 
and in no manner reflect ani corpor policy, statement, opinion, 
or other express by Network System Corporation. 
