I Miner Oil Bad for Skin? 
 
In Thi Article: 
 
What I Miner Oil? 
Environment Concern 
I Miner Oil Contaminated? 
Can Miner Oil Clog Pores? 
The Bottom Line 
Sometim thing that ar good can get a bad reput that� undeserv � and hard to shake! Such i the case with miner oil, a harmless and benefici ingredi that� us in mani skin-car products, but ha been link onlin and in magazin to all kind of terribl things, from clog pore to cancer. It� time to set the record straight on miner oil, and why the neg rumor you�v read or heard ar simpli untrue. 
 
 
What I Miner Oil? 
 
Miner oil i clear, odorless oil deriv from petroleum that i wide us in cosmet becaus it rare caus allerg reaction and it can�t becom solid and clog pores. Studi have found miner oil and petrolatum (a semi-solid form of miner oil more commonli known a Vaseline) can assist in wound healing, and ar among the most effect moistur ingredi available. 
 
Though it associ with petroleum ha caus peopl to sai miner oil i bad for or ag skin, the truth i that petroleum itself i a natur ingredi that come from the earth, and that onc it becom cosmetics- or pharmaceutical-grad miner oil, it bear no resembl to petroleum itself. Miner oil us in cosmet i highli purifi and complet safe. 
 
Environment Concern 
 
Some peopl argu against the us of miner oil, cite concern that process it take to extract it i damag to the environment, and that it i a non-renew resource. First, miner oil i not a resource. It� a byproduct of the petroleum industry. No on i drill for oil to us for moisturizers. Miner oil i extract and purifi from the petroleum refin process that occur within other industries. 
 
Thi i actual no differ from how coconut oil (or other plant oils) i extracted, purifi and process into cosmetics. Thus, if you�r concern with miner oil a a �non-renewable� ingredient, you shouldn�t be. In fact, if thi i your main concern it� more help to stop drive your vehicle, heat your home, cook with gas, travel by airplane, us anyth packag in plastic (or type on a keyboard) etc., all of which ar primari reason petroleum i refined. Obviousli we�r not go to stop do all of those things, nor would it be practical, but you get our point! 
 
I Miner Oil Contaminated? 
 
Miner oil i approv for us in cosmet (and a wide varieti of other medic applications) globally, and in skin-car product i certifi a either USP (Unite State Pharmacopeia) or BP (British Pharmacopeia). It doe not contain impur that harm skin in ani way, nor doe it contain ani carcinogen (cosmetics-grad miner oil i free of the compound present in industri petroleum). 
 
Speak of impurities, you mai be surpris to learn that plant ar subject to contamin a well. Plant come out of the ground, with insects, worms, mold, fungus, bacteria, and other contamin that must be purifi (or removed) off befor thei can be put into a cosmetic, just like miner oil. In fact, if you saw how most plant look (or smell) befor thei undergo thi purification, we bet you�d never want to us anoth natur product again! 
 
Can Miner Oil Clog Pores? 
 
Despit it greasi feel, miner oil can�t clog pore a it cannot penetr skin�it molecular size i simpli too big to get into the pore line where clog happen! Instead�and thi i good news�miner oil remain on the surfac of skin, where it doe the most good�although those with oili skin mai not like how product with a high amount of miner oil feel or look on their skin. 
 
The Bottom Line 
 
The claim that miner oil i unsaf to us ar unfound and ar perpetu by cosmet compani and peopl who us inform about non-purified, industrial-grad miner oil (which isn�t us in skin care) a a scare tactic. The truth i that the miner oil you find in skin- care product i perfectli safe, and even better � veri good for your skin, especi if it� dry or sensitive! 
