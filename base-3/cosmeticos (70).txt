Petroleum-bas cosmet and skin care product found to contain cancer-caus chemic 1,4-dioxan 
 
A recent studi by the non-profit Environment Work Group show that mani cosmet product -- includ more than half of all babi soap -- contain a carcinogen chemical. Internal studi in the cosmet industri show that mani of their product can be contamin by a carcinogen impur call 1,4-dioxane, and the EWG' independ studi show that 1,4-dioxan i fairli widespread among cosmet products. 
 
What you need to know - Convent View 
 
� The studi found 22 percent of all cosmet and skin care product mai be contamin with 1,4-dioxane. 
 
� It also found that 80 percent of all cosmet product mai be contamin with on or more carcinogen impurities. 
 
� In addit to 1,4-dioxane, six other major impur ar hydroquinone, ethylen dioxide, formaldehyde, nitrosamines, PAHs, and acrylamides. 
 
� The EWG analysi found 1,4-dioxan in a wide varieti of cosmet product on the market, includ almost all brand of hair relax and more than half of the babi soap on the market. Contamin level found were a follows: 
97% - hair relax 
 
82% - hair dye and bleach 
 
66% - hair remov 
 
57% - babi soap 
 
45% - sunless tan product 
 
43% - bodi firm lotion 
 
36% - hormon cream 
 
36% - facial moistur 
 
35% - anti-ag product 
 
34% - bodi lotion 
 
33% - around-ey cream 
 
 
� The analysi assess the ingredi list of 15,000 cosmet and other person care products. 
 
� Another impurity, hydroquinone, can potenti contamin the product us daili by 94 percent of all women and 69 percent of all men, the EWG reported. 
 
� To avoid 1,4-dioxane, read ingredi label and avoid ani of the 56 cosmet ingredi that can contain the contaminant, includ "sodium laureth sulfate" and ingredi that includ the claus "PEG," "xynol," "ceteareth," and "oleth." 
 
� "One of everi five adult i potenti expos everi dai to all of the top seven carcinogen impur common to person care product ingredients," the EWG said regard a 2004 study. 
 
What you need to know - Altern View 
 
Statement and opinion by Mike Adams, execut director of the Consum Well Center 
 
� Common, brand-nam skin care product often contain multipl chemic contamin known to caus cancer, liver disord and neurolog disorders. 
 
� I strongli advis consum to avoid us non-organ cosmet or skin care products. Switch to trusted, organ product from compani like Dr. Bronner' (www.DrBronner.com) or Pangea Organic (www.PangeaOrganics.com) 
 
� Rememb that ani cream or cosmet you put on your skin get absorb into your blood. Don't put anyth on your skin that you couldn't safe eat! 
