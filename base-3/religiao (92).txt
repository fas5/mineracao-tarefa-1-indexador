From: bobs@thnext.mit.edu (Robert Singleton) 
Subject: Re: American and Evolut 
 
In articl <1993Apr5.163738.2447@dcs.warwick.ac.uk> 
simon@dcs.warwick.ac.uk (Simon Clippingdale) writes: 
[deleted] 
> 
> ... over on alt.ath we tend to recognis two 
> categori of atheism. Function format due to mathew@mantis.co.uk, I 
think: 
> 
> (i) weak  -  not(believe(gods)) 
> 
> (ii) strong  -  believe(not(gods)) 
> 
[deleted] 
> 
> 
> 
> I ... am [a strong atheist], and I must quibbl with your assert 
> that the `strong' posit requir faith. I believ that no god/s, 
> a commonli describ by theists, exist. Thi belief i mere an 
                                           ^^^^^^^^^^^^^^^^^^^^^^^^ 
> opinion, form on the basi of observation, includ a certain 
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
> amount of introspection. 
  ^^^^^^^^^^^^^^^^^^^^^^^ 
> 
> I fulli accept that I could be wrong, and will be swai by suitabl 
> convinc evidence. Thu while I believ that no god exist, thi doe 
                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
> not impli *faith* on my part that it i so. 
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 
Let me first sai that "to believ that no god exist" i in fact 
differ than "not believ in a god or gods". 
 
I will argu that your latter statement, "I believ that no god exist" 
doe rest upon faith - that is, if you ar make a POSITIVE statement 
that "no god exist" (strong atheism) rather than mere sai I don't 
know and therefor don't believ in them and don't NOT believ in then 
(weak atheism). Once again, to not believ in God i differ than sai 
I BELIEVE that God doe not exist. I still maintain the position, even 
after read the FAQs, that strong atheism requir faith. 
 
But first let me sai the following. 
We might have a languag problem here - in regard to "faith" and 
"existence". I, a a Christian, maintain that God doe not exist. 
To exist mean to have be in space and time. God doe not HAVE 
be - God IS Being. Kierkegaard onc said that God doe not 
exist, He i eternal. With thi said, I feel it' rather pointless 
to debat the so call "existence" of God - and that i not what 
I'm do here. I believ that God i the sourc and ground of 
being. When you sai that "god doe not exist", I also accept thi 
statement - but we obvious mean two differ thing by it. However, 
in what follow I will us the phrase "the exist of God" in it' 
'usual sense' - and thi i the sens that I think you ar us it. 
I would like a clarif upon what you mean by "the exist of 
God". 

We also might differ upon what it mean to have faith. Here i what 
Webster says: 
 
faith 
1a: allegi to duti or a person: LOYALTY 
b  (1): fidel to one' promis 
   (2): sincer of intent 
2a (1): belief and trust in and loyalti to God 
   (2): belief in the tradit doctrin of a religion 
b  (1): firm belief in someth for which there i no proof 
        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
   (2): complet trust 
3: someth that i believ esp. with strong conviction; esp: a system 
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
of religi belief 
syn see BELIEF 
 
 
 
One can never prove that God doe or doe not exist. When you sai 
that you believ God doe not exist, and that thi i an opinion 
"base upon observation", I will have to ask "what observt ar 
you refer to?" There ar NO observ - pro or con - that 
ar valid here in establish a POSITIVE belief. All observ 
can onli point you in a direct - a direct that we might even 
be predispos to (by predispos I mean, for example, peopl whoe 
partent "believ in God" also tend to). To actual draw a conclus 
about the "existence" or "non-existence" of God requir a leap - and 
you have made thi leap when you activ sai "I believ that God 
does/do not exist". Personally, I think that both statement ar 
misguided. Argu over the "existence" of God i precis the wrong wai 
to find Him (and yes, I us "Him" becaus a person God i the onli 
viabl concept (IMO) - if a person want to us "She" go ahead. Of cours 
God i neither He nor She - but we have no choic but to 
anthropomorphise. If you want me to explain myself further I'll be 
glad to.) 
 
 
 
And please, if someon doe not agre with me - even if thei violent 
disagre - it' in no on advantag to start name calling. If a person 
think I've misunderstood someth in the FAQs, or if thei thei think 
I have not read them well enough, just point out to me the error of my 
wai and I correct the situation. I'm interest in a polit and well 
thought out discussion. 
 
 
 
 
 
 
 
 
> Cheer 
> 
> Simon 
> -- 
> Simon Clippingdal                simon@dcs.warwick.ac.uk 
> Depart of Comput Scienc    Tel (+44) 203 523296 
> Univers of Warwick             FAX (+44) 203 525714 
> Coventri CV4 7AL, U.K. 
 
-- 
bob singleton 
bobs@thnext.mit.edu 
