From: johnsd2@rpi.edu (Dan Johnson) 
Subject: Re: The arrog of Christian 
 
In articl 1328@geneva.rutgers.edu, gt7122b@prism.gatech.edu (boundary) writes: 
>dleonar@andy.bgsu.edu (Pixie) writes: 
[deletia- sig] 
>>     p.s.  If you do sincer believ that a god exists, why do you follow 
>>it blindly? 
>>     Do the word "Question Authority" mean anyth to you? 
>>     I defi ani theist to reply. 
> 
 
[deletia- formalities] 
 
I probabl should let thi pass, it' not worth the time, and it' not 
realli intend for me. But I couldn't resist. A person weak of mine. 
Jerkiu Kneeus. Tragic incurable. 
 
>The foundat for faith in God i reason, without which the exist 
>of God could not be proven.  That Hi exist can be proven by reason 
>i indisput (cf. my short treatise, "Tradit Proof for the 
>Existenc of God," and Summa Theologica). 
 
Not so; I can prove that the exist of God i disput 
by show that peopl disput it; Thi i easy: I disput that 
God exists. Simple. 
 
I miss your "Tradit Proofs" treatise, but the proof I rememb 
from the Summa Theolog (the 5 wai I think it was) were rather poor 
stuff. The Ontolog argument i about a billion time better, imho. 
 
I would think you'd want non-tradit proofs, consid the gener 
failur of the tradit proofs: at least the on I know of. 
(I am think of the Ontolog Argument, the Cosmolog Argument and 
the Teleolog argument. Those ar the on tradit enough to 
have funni names, anyway.) 
 
>Now, given that God exists, and that Hi exist can be proven by reason, 
>I assert that Hi command must be follow blindly, although in our fallen 
>condit we must alwai have some measur of doubt about our faith.  Why? 
 
Thi i the real question. So to discuss it, I'll assum God exists. 
Otherwise, there i no heavenli author to babbl about. 
 
>Becaus God i the First Caus of all things, the First Mover of matter, 
>the Independ Thing that requir noth els for it existence, the 
>Measur of all that i perfect, and the essenti Be who give order 
>to the univers (logos). 
 
Pleas show thi i the case. I am familiar with the First Caus 
argument, and I'll accept (for the sake of argument) that there 
i a First Cause, even though I find some of it premic 
questionable. The rest you'll have to show. Thi includ 
that the First Caus i God. 
 
>I next assert that God i all good. 
 
Got it. I deni that God i all good. So there. 
 
>  If thi i so, then that which i 
>contrari to the will of God i evil; i.e., the absenc of the good.  And, 
>sinc God can never contradict Himself, then by Hi promis of a Savior 
>a earli a the Protoevangelium of Genesi 3:5, God instruct that becaus 
>a human (Adam) wa first respons for man' alien from the Sourc 
>of all good, a man would be requir to act to restor the friendship. 
>Thu God becam incarn in the person of the Messiah. 
 
Thi isn't self-consistent: if human must renew the relationship, 
then God (incarn or not) can't do it. Well, unless you think God i 
human. Granted, God made himself 'human', but thi i nonetheless cheating: 
The intent of the statement i clearli that man ha to fix the problem 
he caused. God fix it- even by indirect means- contradict this. 
 
>Now thi Messiah claim that He i the Truth (John 14:6).  If thi claim 
>i true, then we ar bound by reason to follow Him, who i truth incarnate. 
 
Why? 
 
Also, why assum said claim i true anyway? 
 
If *I* claim to be Truth, ar you bound by reason to follow me? 
 
>You next seem to have a problem with authority.  Have you tri the Unite 
>State Marin Corp yet?  I can tell you first-hand that it i an excel 
>instructor in authority. 
 
:) 
 
Undoubtably. Do you mean to impli we should all obei the command of the 
Marin without question? You seem to impli thi about God, and 
that the Marin ar similar in thi respect.. If thi i not what 
you ar try to say, thei pleas explain what it i you ar saying, 
a I have miss it. 
 
>  If you have not yet had the privilege, I will 
>repli that the author which i Truth Incarnat mai never be questioned, 
>and thu must be follow blindly. 
 
Why? Why not question it? Even if it *is* truth, we cannot know thi 
certainly, so why i it so irrat to question? Perhap we will 
thu discov that we were wrong. 
 
You assert that God i Truth and we can't question Truth. But 
I assert that God i not Truth and anywai we can question Truth. 
How i it my assert i less good than yours? 
 
>  One mai NOT deni the truth. 
 
Oh? 
 
I herebi deni 1+1=2. 
 
I hope you'll agre 1+1=2 i the truth. 
 
Granted, I look pretti damn silli sai someth like that, 
but I need someth we'd both agre wa clearli true. 
 
Now, you'll notic no stormtroop have march in to drag 
me off to the gulag. No heaven light bolt either. No mysteri 
net outages. I seem to be permit to sai such things, absurd or not. 
 
>  For 
>example, when the proverbi appl fell on Isaac Newton' head, he could 
>have deni that it happened, but he did not.  The law of physic must 
>be obei whether a human like them or not.  Thei ar true. 
 
Thei ar certainli not true. At least, the on Newton deriv ar 
not true, and ar inde wildli inaccur at high speed or small 
distances. We do not have a set of Law of Physic that alwai 
work in all cases. If we did, Physic would be over already. 
 
Scienc i all about Question thi sort of truth. If we didn't, 
we'd still follow Aristotle. I'd gener thi a littl more: 
If you want to learn anyth new, you MUST question the thing 
you Know (tm). Becaus you can alwai be wrong. 
 
>Therefore, the Author which i Truth mai not be denied. 
 
Even presuppos that Truth mai not be Denied, and mai 
not be Questioned, and that God i Truth, it onli follow 
that God mai not be Deni or Questioned. NOT that he must 
be obeyed! 
 
We could unquestioningli DISobei him. How annoi of us. 
But you have not connect denial with disobedience. 
 
--- 
			- Dan "No Nickname" Johnson 
And God said "Jeeze, thi i dull"... and it *WAS* dull. Genesi 0:0 
 
These opinion probabl show what I know. 
