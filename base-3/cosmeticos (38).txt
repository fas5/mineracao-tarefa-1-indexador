Help for Oili Skin 
 
In Thi Article: 
 
Recommend Product for Oili Skin 
Recogn and Understand Oili Skin 
Care for Oili Skin 
Step-by-Step Routin for Oili Skin 
If you struggl with the daili annoy of a shini face that resembl an oil slick, you know how difficult it i to get oili skin under control. Adding to your frustration, whether you know it or not, i the fact that that mani of the product claim to elimin shine and reduc oil actual make matter wors becaus thei contain ingredi that irrit your skin and trigger more oil production! 
 
Recogn and Understand Oili Skin 
 
Oili skin i hard to control becaus it' the result of genet determin hormon chang in your body, and you simpli cannot control hormon topically. The hormon respons for oili skin ar call androgens�th male hormones�and thei ar present in both men and women. 
 
Androgen stimul healthi oil production, and while that truli ha benefit for your skin, it i a problem when androgen stimul too much oil to be produced! When too much oil i produced, the pore becom larger to accommod the excess oil production. Excess androgen can also caus the pore line to thicken, which block oil from get out of the pore, and that can result in blackhead and white bumps. 
 
Not sure if you have oili skin? It' recogniz by a few classic characteristics: 
 
Your face i shini onli an hour or two after cleansing, and usual appear greasi by midday. 
Your makeup seem to "slide," or disappear right off your face. 
The more oili area of your face have blackheads, white bumps, or acne. 
The pore ar visibli enlarged, especi on your nose, chin, and forehead. 
Care for Oili Skin 
 
The first step in care for oili skin i to take a critic look at your current skin-car routine. Using product with dry or irrit ingredi mai seem like a good idea becaus thei make your skin feel less oily, at least initially, but in the long run us such ingredi i a bad idea. 
 
Irritat or dry ingredi onli make matter worse, becaus thei actual trigger more oil to be produc directli in the oil gland! Our advice: Avoid irrit ingredi at all costs! 
 
Product that make your skin tingl (such a menthol, mint, eucalyptus, and lemon) or that contain alcohol mai feel like thei ar help with your oili skin, but tingl i not help for anyone' skin. When your skin tingles, it mean it i be irritated, and irrit i alwai bad for skin. Tingl i just on wai your skin i tell you it i hurting, and the cumul damag will end up caus more problems. 
 
Find out which irrit ingredi everyon should avoid. 
 
Also bad for oili skin ar product that contain pore-clog or emolli ingredients, which mai make your oili skin worse. A a gener rule, ingredi that keep bar product in solid form (such a bar cleanser and soap or stick foundations), or that ar present in emolli lotion and cream ar like to clog pore and look greasi on your skin. 
 
Instead of us cream or thick lotions, consid us onli liquid, serum, or gel formulations. 
 
Step-by-Step Routin for Oili Skin 
 
The follow essenti skin-car guidelines�cleanse, tone, exfoliate, A.M. sun protection, P.M. hydration, and absorb excess oil�wil help you take control of your skin so you'll see less oil, smaller pores, and fewer breakouts, us product from Paula' Choic or the other product we recommend on Beautypedia. 
 
1. Cleans 
 
Use a gentle, water-solubl cleanser twice daily. Ideally, the cleanser should rins without leav a hint of residue, should not contain dry cleans agent such a sodium lauryl sulfat (dry up skin doesn't help anything), and should be fragrance-fre (fragranc i alwai irritating) 
 
2. Tone 
 
An alcohol-fre toner load with antioxid and cell-commun ingredi i an import step for oili skin. Toner that contain these ingredi help skin heal, minim larg pore by reduc inflammation, and remov the last trace of dead skin cell or makeup that can lead to clog pores. 
 
3. Exfoliat 
 
Exfoliat i on of the most import skin-car step for oili skin. Oili skin tend to have an extra-thick layer of built-up dead skin cell on the surfac of the skin, along with a thicken pore lining. Exfoliat i the best wai to remov that build up, reduc clog pore and white bumps, while make skin feel smoother. 
 
The best exfoli ingredi for oili skin i salicyl acid (BHA). Salicyl acid exfoli not onli the surfac of your skin but also insid the pore lining, thu improv pore function and allow oil to flow easili to the surface, so it doesn't get back up and plug the pore. In addition, over time, regular us of a BHA exfoli will help fade the red mark from past blemishes. 
 
Another benefit of salicyl acid i that it ha anti-inflammatori properties, so it reduc irritation, which help to calm oil production. Paula' Choic offer sever BHA products. 
 
4. A.M. Sun Protect 
 
Even if you have oili skin, a sunscreen i essenti for prevent wrinkl and reduc red marks. If you'v avoid sunscreen becaus the on you'v tri ar too greasi or too occlusive, we have some option that will chang your impress of sunscreen for good. Look for weightless protect that help keep your skin matte. You also can consid appli a matte-finish liquid foundat rate SPF 25 or greater and a press powder with SPF 15 or greater. 
 
5. P.M. Hydrat 
 
At night, choos a lightweight liquid, gel, or serum that contain no pore-clog ingredi and that can provid hydrat while treat your skin to the essenti ingredi all skin type need to function in a normal, healthi manner, such a antioxidant, cell-communicating, and skin-repair ingredients. 
 
6. Absorb Excess Oil 
 
A you begin to get your oili skin under control, it' like that you still will need to us oil-absorb products, mayb weekly, biweekly, or even daily. Thi i an option step, but mani with oili skin find it helpful. Our favorit trick for nix excess oil i to first blot with oil-blot papers, and follow it with a light dust of press powder with SPF 15. (Bonus: You'r ad to your sun protection!) 
