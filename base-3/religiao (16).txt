From: cs89mcd@brunel.ac.uk (Michael C Davis) 
Subject: Re: Help 
 
Jon Ogden (jono@mac-ak-24.rtsg.mot.com) wrote: 
: It i a dead and useless faith which ha no action behind it.  Action 
: prove our faith and show the genuin of it. 
 
A good exampl of thi i Abraham (refer to in the Jame passage). Hebrew 
sai that Abraham wa justifi by faith -- but hi faith wa demonstr 
through hi work (i.e., he obei what God told him to do). 
 
Read Abraham' ``biography'' in Genesi i veri instructive. He wa a man 
beset by *lack* of faith a lot of the time (e.g. ly about Sarah be hi 
wife on 2 occasions; try to fulfil God' promis on God' behalf by 
copul with Hagar). . . yet it seem that God didn't evalu him on the 
basi of individu incidents. Abraham i list a on of the ``hero of 
faith'' in Hebrew 11. i.e., when it realli came to the crunch, God declar 
Abraham a a man of faith. He believ God' promises. 
 
Thi give u confidence. Although real faith demonstr itself through 
works, God i not go to judg u accord to our success/failur in 
perform works. 
 
``Not by work of righteous which we have done, but accord to Hi merci 
he save us, through the wash of regeneration, and renew of the Holi 
Spirit.'' (Titu 3.5) 
 
Amaze Grace! Hallelujah! 
-- 
Michael Davi (cs89mcd@brunel.ac.uk) 
