package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.tartarus.snowball.ext.PorterStemmer;

public class TratarBases {
	
	public static void criar(boolean removerStopwords, boolean tratarStemming) {
		if(removerStopwords && !tratarStemming) {
			processarArquivos(Constantes.OPERACAO_STOPWORD, Constantes.BASE_1, Constantes.BASE_2);
		} else if(!removerStopwords && tratarStemming) {
			processarArquivos(Constantes.OPERACAO_STEMMING, Constantes.BASE_1, Constantes.BASE_3);
		} else if(removerStopwords && tratarStemming) {
			processarArquivos(Constantes.OPERACAO_STOPWORD, Constantes.BASE_1, Constantes.BASE_4);
			processarArquivos(Constantes.OPERACAO_STEMMING, Constantes.BASE_4, Constantes.BASE_4);
		}
	}
	
	private static void processarArquivos(String operacao, String diretorioOrigem, String diretorioDestino) {
		PorterStemmer p = new PorterStemmer();
		StringBuilder stringBuilder = null;
		BufferedReader br = null;
		FileReader fr = null;
		
		File[] listaArquivos = new File(diretorioOrigem).listFiles();
		
		File dirDestino = new File(diretorioDestino);
		if(!dirDestino.exists()) {
			dirDestino.mkdirs();
		}
		
		for(File file : listaArquivos) {
			try {
				stringBuilder = new StringBuilder();				
				fr = new FileReader(file.getAbsolutePath());
				br = new BufferedReader(fr);
				
				String sCurrentLine;
				while ((sCurrentLine = br.readLine()) != null) {
					String[] linha = sCurrentLine.split(" ");
					
					for(String s : linha) {
						stringBuilder = operacao.equals(Constantes.OPERACAO_STOPWORD) ? 
								linhaStopWords(s, stringBuilder) : linhaStemming(p, s, stringBuilder);
					}
					
					stringBuilder.append("\r\n");
				}
								
				FileOutputStream out = new FileOutputStream(new File(diretorioDestino.concat("/") + file.getName()));
				out.write(stringBuilder.toString().getBytes());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null){
						br.close();
					}						
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	private static StringBuilder linhaStopWords(String s, StringBuilder stringBuilder) {
		if(!Constantes.STOPLIST.contains(s.toLowerCase())) {
			stringBuilder.append(s);
			stringBuilder.append(" ");
		}
		
		return stringBuilder;
	}
	
	private static StringBuilder linhaStemming(PorterStemmer p, String s, StringBuilder stringBuilder) {
		p.setCurrent(s);
		p.stem();
		stringBuilder.append(p.getCurrent());
		stringBuilder.append(" ");
		
		return stringBuilder;
	}
	
	public static void main(String[] args) {
		boolean removerStopwords = false;
		boolean tratarStemming = true;
		
		criar(removerStopwords, tratarStemming);
	}
}