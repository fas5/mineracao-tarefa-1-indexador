package util;

import java.util.ArrayList;
import java.util.Arrays;

import org.tartarus.snowball.ext.PorterStemmer;

public class TratarQuery {
	
	public static String extrairRadicais (String pesquisa){
		String[] pesquisaArray = pesquisa.split(" ");
		StringBuilder sb = new StringBuilder();
		PorterStemmer p = new PorterStemmer();
		
		for(String s : pesquisaArray) {
			p.setCurrent(s);
			p.stem();
			sb.append(p.getCurrent());
			sb.append(" ");
		}
		
		return sb.toString();
	}

	public static String excluirStopwords(String pesquisa) {
		String[] pesquisaArray = pesquisa.split(" ");
		StringBuilder sb = new StringBuilder("");
		
		for(String s : pesquisaArray) {
			if(Constantes.OPERADOR_AND.equals(s) || !Constantes.STOPLIST.contains(s.toLowerCase())) {
				sb.append(s);
				sb.append(" ");
			}
		}
		
		String novaPesquisa = sb.toString();
		ArrayList<String> novaPesquisaList = new ArrayList<String>(Arrays.asList(novaPesquisa.split(" ")));

		sb = new StringBuilder("");
		for(int i = 0; i < novaPesquisaList.size() - 1; i++) {
			for(int j = 0; j < novaPesquisaList.size() - 1; j++) {
				if(Constantes.OPERADOR_AND.equals(novaPesquisaList.get(j)) && 
						Constantes.OPERADOR_AND.equals(novaPesquisaList.get(j+1))) {
					novaPesquisaList.remove(j+1);
				}
			}
		}
		
		for(String s : novaPesquisaList) {
			sb.append(s);
			sb.append(" ");
		}
		
		novaPesquisa = sb.replace(sb.length()-1, sb.length(), "").toString();
		if(novaPesquisa.startsWith(Constantes.OPERADOR_AND)) {
			novaPesquisa = novaPesquisa.substring(4, novaPesquisa.length());
		}
		if(novaPesquisa.endsWith(Constantes.OPERADOR_AND)) {
			novaPesquisa = novaPesquisa.substring(0, novaPesquisa.length()-4);
		}
			
		return novaPesquisa;
	}
	
	public static void main(String[] args) {
		System.out.println(excluirStopwords("AND The AND always AND all AND me AND forever AND"));
	}
	
//	public static void main(String[] args) {
//	String strings[] = {"hidden", "waiting", "swimming", "lending"};
//	
//	PorterStemmer p = new PorterStemmer();
//	
//	for(String s : strings) {
//		p.setCurrent(s);
//		p.stem();
//		System.out.println(p.getCurrent());
//	}		
//}
}
