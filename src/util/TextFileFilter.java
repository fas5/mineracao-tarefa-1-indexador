package util;

import java.io.File;
import java.io.FileFilter;

public class TextFileFilter implements FileFilter {

	@Override
	public boolean accept(File path) {
		return path.getName().toLowerCase().endsWith(".txt");
	}
}