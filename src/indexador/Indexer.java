package indexador;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import util.Constantes;

public class Indexer {
	private IndexWriter writer;
	
	public Indexer(String indexDirectoryPath, boolean stopWords, boolean stemming) 
			throws IOException {
		
		Directory indexDirectory = 
				FSDirectory.open(new File(indexDirectoryPath).toPath());
		
		StandardAnalyzer analyzer;
		
		if(stopWords) {
			CharArraySet stopList = new CharArraySet(Constantes.STOPLIST, true);			
			analyzer = new StandardAnalyzer(stopList);
		} else {
			analyzer = new StandardAnalyzer(CharArraySet.EMPTY_SET);
		}
		
		analyzer.setVersion(Version.LATEST);
		
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		
		this.writer = new IndexWriter(indexDirectory, config);
		this.writer.deleteAll();
	}
	
	public boolean close() throws IOException {
		this.writer.close();
		
		return this.writer.isOpen();
	}
	
	private Document getDocument(File file) 
			throws FileNotFoundException, IOException {
		
		Field contentField = new TextField(
				Constantes.CONTENTS, 
				new FileReader(file));
		Field fileNameField = new StringField(
				Constantes.FILE_NAME, 
				file.getName(), 
				Field.Store.YES);
		Field filePathField = new StringField(
				Constantes.FILE_PATH, 
				file.getCanonicalPath(), 
				Field.Store.YES);
		
		Document document = new Document();
		document.add(contentField);
		document.add(fileNameField);
		document.add(filePathField);
		
		return document;
	}

	private void indexFile(File file) {
		try {
			//System.out.println("Indexando " + file.getCanonicalPath());
			
			Document document = getDocument(file);
			this.writer.addDocument(document);
			
			//System.out.println("Arquivo indexado!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public int createIndex(String dataDirPath, FileFilter filter) {
		File[] files = new File(dataDirPath).listFiles();
		
		for(File file : files) {
			if(!file.isDirectory() 
					&& !file.isHidden() 
					&& file.exists() 
					&& file.canRead() 
					&& filter.accept(file)) {
				
				indexFile(file);
			}
		}
		
		return this.writer.numDocs();
	}
}
