package lucene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import buscador.Searcher;
import indexador.Indexer;
import util.Constantes;
import util.TextFileFilter;

public class LuceneFacade {
	private final String INDEX_DIR = "index";
	private Indexer indexer;
	private Searcher searcher;
	private String base;
	private boolean stopWords;
	private boolean stemming;
	
	public LuceneFacade(boolean stopWords, boolean stemming) {
		this.stopWords = stopWords;
		this.stemming = stemming;
		
		if(!stopWords && !stemming) {
			this.base = Constantes.BASE_1;
		} else if(stopWords && !stemming) {
			this.base = Constantes.BASE_2;
		} else if(!stopWords && stemming) {
			this.base = Constantes.BASE_3;
		} else if(stopWords && stemming) {
			this.base = Constantes.BASE_4;
		}
	}
	
	public void createIndex() throws IOException {
		System.out.println("[INDEXANDO DOCUMENTOS]");
		
		int numIndexed;
		
		long startTime = System.currentTimeMillis();
		
		this.indexer = new Indexer(this.INDEX_DIR, this.stopWords, this.stemming);
		numIndexed = this.indexer.createIndex(this.base, new TextFileFilter());
		
		long endTime = System.currentTimeMillis();
		
		this.indexer.close();
		
		System.out.printf("%d arquivo(s) indexado(s) em %dms.\n", numIndexed, (endTime-startTime));
	}
	
	public List<String> pesquisar(String searchQuery) throws IOException {
		System.out.println("[BUSCANDO DOCUMENTOS]");
		
		long startTime = System.currentTimeMillis();
		List<String> documentosEncontrados = new ArrayList<String>();
		
		this.searcher = new Searcher(this.INDEX_DIR, this.stopWords, this.stemming);
		TopDocs hits = this.searcher.search(searchQuery);
		
		long endTime = System.currentTimeMillis();
		
		System.out.printf("%d documento(s) encontrado(s) em %dms.\n", hits.totalHits, (endTime-startTime));
		
		for(ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = searcher.getDocument(scoreDoc);
			documentosEncontrados.add(doc.get(Constantes.FILE_NAME));
		}
		
	    this.searcher.close();
	    return documentosEncontrados;
	}	
}
