package lucene;

import java.io.IOException;

public class MainTeste {
	
	private final static boolean stopWords = false;
	private final static boolean stemming = false;
	
	public static void main(String[] args) {
		LuceneFacade li = new LuceneFacade(stopWords, stemming);
		
		try {
			li.createIndex();
			li.pesquisar("is");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
