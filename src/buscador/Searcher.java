package buscador;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import util.Constantes;

public class Searcher {
	private IndexReader indexReader;
	private IndexSearcher indexSearcher;
	private QueryParser queryParser;
	private Query query;
	
	public Searcher(String indexDirectoryPath, boolean stopWords, boolean stemming) 
			throws IOException {
		
		this.indexReader = DirectoryReader.open(
				FSDirectory.open(new File(indexDirectoryPath).toPath()));
		this.indexSearcher = new IndexSearcher(this.indexReader);
		
		StandardAnalyzer analyzer;
		
		if(stopWords) {
			CharArraySet stopList = new CharArraySet(Constantes.STOPLIST, true);
			analyzer = new StandardAnalyzer(stopList);
		} else {
			analyzer = new StandardAnalyzer(CharArraySet.EMPTY_SET);
		}
		
		analyzer.setVersion(Version.LATEST);
		
		this.queryParser = new QueryParser(Constantes.CONTENTS, analyzer);
	}
	
	public TopDocs search(String searchQuery) {
		TopDocs results = null;
		try {
			this.query = queryParser.parse(searchQuery);
			results = indexSearcher.search(query, Constantes.MAX_SEARCH);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
	public Document getDocument(ScoreDoc scoreDoc) {
		Document document = null;
		try {
			document = indexReader.document(scoreDoc.doc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return document;
	}
	
	public void close() throws IOException{
		this.indexReader.close();
	}
}
