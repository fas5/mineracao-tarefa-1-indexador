package gui;

import java.awt.Button;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import lucene.LuceneFacade;
import util.TratarQuery;

public class View {

	private JFrame frame;
	private JTextField textoPesquisar;
	private JLabel tituloPerguntaLabel;
	private Button botaoPesquisar;
	
	private Vector<String> columnNames;
	private JScrollPane scrollPane;
	private JCheckBox stopWordCheckBox;
	
	private JCheckBox stemCheckBox;	
	
	private JTable table;
	public static DefaultTableModel dtm;
	private JLabel lblQuantidadeDeDocumentos;
	private JTextField qtdeDocumentostextField;
	
	public View() {
		initialize();
	}

	private void initialize() {
		
		frame = new JFrame();
		frame.setTitle("Not Google!");
		frame.setBounds(200, 200, 550, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		tituloPerguntaLabel = new JLabel("O que deseja pesquisar?");
		tituloPerguntaLabel.setBounds(210, 32, 166, 14);
		frame.getContentPane().add(tituloPerguntaLabel);
		
		textoPesquisar = new JTextField();
		textoPesquisar.setBounds(29, 57, 470, 20);
		frame.getContentPane().add(textoPesquisar);
		textoPesquisar.setColumns(10);
		textoPesquisar.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = -8098045441051180756L;

			@Override
			public void actionPerformed(ActionEvent e) {
				pesquisar();				
			}
		});
		
		table = new JTable();
		
		dtm = new DefaultTableModel(0, 2);
		columnNames = new Vector<String>();
		columnNames.add("#");
		columnNames.add("Nome do arquivo");
		dtm.setColumnIdentifiers(columnNames);
		table.setModel(dtm);
		// Setando tamanho das colunas:
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		// Coluna '#':
		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(0).setPreferredWidth(30);
		// Coluna 'Documentos':
		table.getColumnModel().getColumn(1).setMinWidth(437);
		table.getColumnModel().getColumn(1).setPreferredWidth(437);
		
        scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(29, 173, 470, 557);
		table.setBounds(10, 250, 1000, 705);
		scrollPane.setViewportView(table);
		frame.getContentPane().add(scrollPane);
		
		lblQuantidadeDeDocumentos = new JLabel("Quantidade de documentos encontrados:");
		lblQuantidadeDeDocumentos.setBounds(29, 125, 253, 14);
		frame.getContentPane().add(lblQuantidadeDeDocumentos);
		
		qtdeDocumentostextField = new JTextField();
		qtdeDocumentostextField.setEditable(false);
		qtdeDocumentostextField.setBounds(280, 122, 86, 20);
		frame.getContentPane().add(qtdeDocumentostextField);
		qtdeDocumentostextField.setColumns(10);
		
		stemCheckBox = new JCheckBox("Stemming");
		stemCheckBox.setBounds(29, 84, 97, 23);
		frame.getContentPane().add(stemCheckBox);
		
		stopWordCheckBox = new JCheckBox("StopWord");
		stopWordCheckBox.setBounds(136, 84, 97, 23);
		frame.getContentPane().add(stopWordCheckBox);
		
		botaoPesquisar = new Button("Pesquisar");
		botaoPesquisar.setBounds(429, 85, 70, 22);
		botaoPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pesquisar();
			}
		});
		
		
		frame.getContentPane().add(botaoPesquisar);
	}
	
	private void pesquisar() {
		// BASE 1 - SEM ELIMINAR STOPWORDS, SEM USAR STEMMING
		// walking with my dog
		// BASE_2 - ELIMINAR STOPWORDS, SEM USAR STEMMING
		// walking my dog
		// BASE_3 - SEM ELIMINAR STOPWORDS, USANDO STEMMING
		// walk with my dog				
		// BASE_4 - ELIMINAR STOPWORDS e USANDO STEMMING
		// walk my dog
		
		String pesquisa = textoPesquisar.getText();
		
		if(!pesquisa.trim().isEmpty()) {
			LuceneFacade li = new LuceneFacade( 
					stopWordCheckBox.isSelected(), 
					stemCheckBox.isSelected());
			List<String> listaDocsEncontrados = new ArrayList<String>();
			
			// Tratar stopwords?
			if(stopWordCheckBox.isSelected()) {
				pesquisa = TratarQuery.excluirStopwords(pesquisa);
			}
			
			// Tratar stemming?
			if(stemCheckBox.isSelected()) {
				pesquisa = TratarQuery.extrairRadicais(pesquisa);
			}
			
			// Remover linhas da pesquisa anterior:
			for(int i = dtm.getRowCount() - 1; i >= 0; i--) {
				dtm.removeRow(i); 
			}
			
			try {
				li.createIndex();
				listaDocsEncontrados = li.pesquisar(pesquisa);
				
				Collections.sort(listaDocsEncontrados);
				
				List<String> lista = Arrays.asList(
						"3","5","7","8","9","13","21","22","23","26",
						"33","34","39","42","43","49","51","52","55","56",
						"59","60","62","64","66","68","70","73","75","78",
						"79","82","85","88");
				
				int counter = 0;
				
				for(String s : lista) {
					for(String doc : listaDocsEncontrados) {
						if(doc.contains("religiao") && doc.contains("("+s+")")) {
							counter++;
							continue;
						}
					}
				}
				
				System.out.println(counter);
				
				for(int i= 0; i < listaDocsEncontrados.size(); i++ ) {
					dtm.addRow(new Object[] { i+1, listaDocsEncontrados.get(i) });
				}
				
				qtdeDocumentostextField.setText(String.valueOf(listaDocsEncontrados.size()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View window = new View();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
