Cosmet Skin Deep 

Lotion, deodorant, hair spray, nail polish, sunscreen ... . spend least amount pouring, scrubbing, patting, lather cleaner, softer hair, skin, body. can't pronounc ingredi product slather on, shelv popular stores. Doesn't mean they'r safe use? live million us product contain hundr chemic approv safeti regulatori institutions. additives, however, actual proven unsafe. 

skin largest organ body. distinct line defens immun system. skin absorb blood stream, doesn't sens us topic affect health? 

Cosmet big business. Numer product men, women, children, babi spend million dollar year. exactli pai for? 

fact 20% chemic product slather sprai bodi assess safeti industry' safeti panel. 1938, Unite State Food Drug Administr (FDA) ban nearli 12,000 ingredi us cosmetics, neighbor European nation ban 1,100. 

Current FDA doe test safeti person care products, law requir ingredi list labels. example, fragranc alon consist hundr individu ingredi remain unspecifi consumer' knowledge. Decis regard ingredi us product - level ingredi - discret Cosmet Industry. 

amount chemic product small harm own, consid product contain addit often us daili basis. Multipli initi low amount number time might us singl day, number us year, amount exposur becom considerable. 

Ingredient item us bodi form contain chemic link cancer, immun system damage, learn disabilities, asthma, damag sperm, name few. 

cosmet industri market product help look, feel, smell better. happening? coal tar hair dyes. exactli Sodium Lauryl Sulfate, Dibutyl Phthalat 1,4-dioxane? organs, skin, bodi tissu respond ingredients? chemicals, hundr more, includ popular cosmet product contain carcinogens, reproduct development toxins, endocrin disruptor link birth defects. 

land cosmetics, "herbal," "natural," "organic" legal definition, see label doe necessarili mean chemic ingredi ad them. doe mean ingredi list product unsafe. certainli product sold contain chemical-fre ingredients. 

