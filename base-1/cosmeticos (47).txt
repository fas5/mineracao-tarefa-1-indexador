How to Care for Dry Skin

In This Article:

Recommended Products
Why Your Skin Gets Dry
How to Fix Dry Skin
Will Drinking Water Help Dry Skin?
As odd as it sounds, dry skin isn't about a lack of moisture. Studies comparing the water content of dry skin to normal or oily skin don't show a significant difference between them. In fact, healthy skin is only about 30% water.

Adding more moisture to dry skin is not necessarily a good thing. If anything, too much moisture, such as from soaking in a bathtub, is bad for skin because it disrupts the skin's outer layers making it dry, flaky, dehydrated, and crepey.

Why Your Skin Gets Dry

The primary reason your skin becomes dry is all about impairment. What happens is that the outer layers of skin lose their ability to maintain normal moisture levels. For the most part, this is due to sun damage and, to some extent, the use of skin-care products that contain irritating or drying ingredients.

Have you ever noticed that the parts of your body that don't have sun damage (the parts of your body that are not routinely exposed to the sun) are rarely, if ever, dry (just look at the inside part of your arm or your derriere)? That's because areas with little to no sun damage don't suffer the range of problems that stem from cumulative sun exposure.

How to Fix Dry Skin

What dry skin needs is barrier-repairing ingredients that help it act younger so it can sustain a healthy water balance. Here are our best tips to stop the cycle of dry, uncomfortable skin:

The first step is to stop damaging the outer layer of skin by avoiding:

Soaps (all kinds, and anything in bar form)
Harsh cleansing ingredients such as sodium lauryl sulfate or sodium C14-16 olefin sulfonate
Products with irritating ingredients (i.e. alcohol, peppermint, menthol, mint, citrus, eucalyptus, fragrance)
Exposure to hot water
Abrasive scrubs (think of the microdermabrasion-at-home type, or those that contain fruit and nut fragments)
Loofahs
The next steps are to give your skin what it needs to act like it did before it was damaged by the sun. Here are some fail-safe tips to help you have beautifully smooth, younger-looking skin:

Wear sunscreen daily, even in winter: Sun damage slowly makes your skin less and less able to hold moisture or feel smooth.
Use state-of-the-art moisturizers (serums, gels, lotions, creams, anti-wrinkle, anti-aging, firming, etc. are all just "moisturizers"): Moisturizers should be filled to the brim with antioxidants, ingredients that help skin hold on to water, skin-repairing ingredients, and anti-inflammatory ingredients. To ensure stability of the light- and air-sensitive ingredients, make sure whatever moisturizer you choose does not come in clear or jar packaging.
Exfoliate: Skin cell turnover (exfoliating) is a function of healthy skin, but due to sun damage, your skin needs help with this process. A well-formulated alpha hydroxy acid (AHA) or beta hydroxy acid (BHA) product can help skin cells turn over in a more natural, youthful manner by removing the build-up of old skin cells and replacing them with newer, smoother ones.
For very dry skin, use pure plant oils such as olive oil or jojoba seed oil, applied after your moisturizer over dry areas.
Don't forget your lips: Lips are the least capable of staying smooth and soft. Do not leave your lips naked day or night. During the day it is essential to apply and reapply an emollient lipstick or gloss. At night, do not go to sleep without protecting your lips with an emollient lip balm all year-round.
Never use products that contain drying or irritating ingredients: But you already knew that one, right?
Constant exposure to dry environments, cold weather, or air from heaters or air conditioners can also be problematic because they impair and degrade the skin's outer protective layer. Adding a humidifier to your home can make a world of difference!
Will Drinking Water Prevent Dry Skin?

This is one of those beauty myths that refuses to go away, but here are the facts: Although drinking eight glasses of water a day is great for your body, it doesn't work to improve or reduce dry skin. If all it took to get rid of dry skin was drinking more water, then no one would have dry skin and moisturizers would stop being sold.

The causes of and treatments for dry skin are far more complicated than just drinking water. Of course, you should be careful to not consume the types of liquids that lead to dehydration, with alcohol and caffeine being the chief offenders. Balancing moderate intake of coffee or, for example, red wine with water will help offset the dehydrating effects both can have on your body.