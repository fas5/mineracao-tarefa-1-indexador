Are Spa Products Worth Your Money?

In This Article:

Recommended Products
Spa Products Contain Irritants, Too
From Truffles to Mud Masks
Spa products are not the result of any particular enlightenment, nor are they specially formulated in any way. In fact, the "spa" formulations for various body washes, bath salts, body moisturizers, and bath oils are really almost identical to the drugstore formulations. Moreover, spa lines, like most body-care products, are highly fragranced, and fragrance is no better for the body than it is for the face.

Spa Products Contain Irritants, Too

Far be it from us to deny anyone the pleasure that can be derived from pampering, soothing body care. Taking care of the body should include relaxing in deliciously warm water or being gently massaged. But skin-care problems require more than sweetly scented bath salts and oils, the very same things that are often responsible for red, flaky, irritated skin. Despite the allure of spa treatments, the products used rarely, if ever, seriously address the issues of sun damage, blemishes, skin sensitivity, antioxidants, anti-irritants, or skin disorders such as rosacea or psoriasis. More often than not, they treat skin to a huge amount of irritating fragrance, especially from the so-called "essential" oils. Despite this, you'd be hard-pressed to find a spa that offers services using only fragrance-free products.

From Truffles to Mud Masks

Doing all they can to appear more natural and superior to other product lines, spa lines tend to add an eccentric flare. In marketing their services and products, one line boasted their products contained truffles that were handpicked and mixed into their treatments. While handpicked truffles (is there another kind?) might be great for a salad or steak, the skin can't tell the difference.

What does matter is whether or not truffles can have a positive effect on skin. It turns out that there is a small amount of research showing black and white truffles to be effective for inhibiting melanin production and for some antibacterial properties (Sources: Federation of European Microbiological Societies (FEMS) Microbiology Letters, April 2000, pages 213319; Pigment Cell Research, February-April 1997, pages 4653). However, there are many different products that contain other effective skin-lightening or antibacterial ingredients. And simply massaging truffles in an oil base over skin won't lead to long-lasting results on dark spots.

The herald for almost all spa treatments is the ever-popular mud mask. Foreign mud is the concept that gets the consumer's attention, because how could mud from the United States or Canada be worth $50 to $100 a jar or per treatment? How appealing would mud from Idaho be in comparison to mud from Ishia or Austria?

"Parafango" mud gets a lot of attention in spa treatments, but "parafango" is merely Italian for "protecting mud"it isn't a kind of special earth from some distant, far-off land. But could there be parts of the world where the dirt is better for your skin than it is from somewhere else? Well, we have looked for the evidence, and to tell you that there is no research showing mud that is beneficial for skin is an understatement. Further, having a mask applied once a month at a spa is similar to dieting once a month; it ends up being of little help in keeping your body healthy day in and day out. It may feel good to be wrapped or to soak in mud, but the benefit is purely emotional and unrelated to any real effect on your skin. If it relaxes your mind, great, but realistically that (and the mess of washing all that mud off) is about all you can expect in terms of benefits!

The bottom line is that spa services can be wonderful and are often a welcome respite from a busy life. We advise you to take advantage of all the massaging and pampering you like, but don't feel the least bit guilty for leaving the spa without a bag of products (and big receipt) in tow!