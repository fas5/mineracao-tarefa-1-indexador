6 Perfect Sunscreens for Oily Skin

In This Article:

The Oily-Skin Guidelines for Choosing the Right Sunscreen
6 Ultra-Light Sunscreens for Oily Skin
Recommended Products
By now, everyone knows�or should know�that sunscreen is the best �anti-wrinkle� cream on the market. Daily use of a broad-spectrum sunscreen rated SPF 25 or greater is the best way to prevent premature skin aging, brown discolorations, and loss of radiance.

We hear you, if only that were the end of the story, right? If you have oily skin (especially if you�re prone to breakouts), you likely know from experience that finding the right sunscreen can be a challenging task�yet using it is important for all the reasons mentioned above and because protecting skin from sun damage speeds healing of red marks from past breakouts.

Fortunately, we�ve done the work for you and found six sunscreens that are among the best for your tricky skin type. These superbly sheer, matte finish sunscreens were plucked from Beautypedia Reviews for their antioxidant-loaded and exceptionally sheer formulas; all perform beautifully under makeup.

The Oily-Skin Guidelines for Choosing the Right Sunscreen

Before we get to our favorites, let�s start with a few pointers you can rely on to help make your sunscreen shopping trip easier!

Look for lightweight sunscreens (sheer or fluid lotions) that dry down to a matte finish. Well-formulated water-based sunscreens with absorbent ingredients like silica or isododecane are prime candidates!
Skip formulas that are loaded with emollients like plant oils and fatty acids, which often result in a shiny face (and makeup that seems to slide off your face by lunch).
Stick to broad-spectrum formulas rated SPF 25 or greater. Ample research has now demonstrated it�s best to use higher SPF levels, as many tend to skimp on the amount of sunscreen they apply (thus, shorting them of the SPF rating on the label).
We don�t recommend relying on your makeup as your sole source of sunscreen�unless you�re willing to pile it on (please don�t), as few of us will ever apply enough foundation, tinted moisturizer or BB cream to get the amount of SPF stated on the label.
When possible, look for sunscreens that include extra antioxidants and anti-irritants, as research has demonstrated antioxidants improve sunscreen�s ability to protect your skin from UV exposure (Journal of Cosmetic Laser Therapy, 2010).
For more sunscreen frequently asked questions, (like how often you should you reapply your sunscreen + more) check out our article, How to Apply Sunscreen.

6 Ultra-Light Sunscreens for Oily Skin

Each of the sunscreens we�ve selected fit the above criteria and work beautifully for those with oily to combination skin types (including the breakout-prone).  Ultimately, only you can determine whether a sunscreen is the right match for your personal aesthetic preferences, but these six options are among some of the best choices for your skin type:

Paula�s Choice Resist Daily Youth-Extending Fluid SPF 50 ($32 for 2 fl. oz.). This water-like formula feels like nothing on skin yet is loaded with soothing ingredients and antioxidants.
philosophy miracle worker spf 50 miraculous anti-aging fluid ($58 for 1.7 fl. oz.). While the price isn�t miraculous, this is a standout formula from philosophy and it�s fluid-lotion texture feels sublime on oily skin.
Paula�s Choice Resist Super-Light Daily Wrinkle Defense SPF 30 ($32 for 2 fl. oz.). A sheer, skin-enhancing tint offsets the white-cast of its zinc oxide formula that sets to a super-matte finish (no need for primer!).
Kate Somerville Serum Sunscreen SPF 50+ ($45 for 2 fl. oz.). Despite the �serum� name, this thin lotion deposits a light amount of moisture without a greasy finish.
Paula�s Choice Skin Balancing Ultra Sheer Daily Defense SPF 30 ($28 for 2 fl. oz.). Making good on its name, this �ultra-sheer� formula helps control oil while drying to a soft-matte feel on skin.
Olay Regenerist Regenerating Lotion with Sunscreen SPF 50 ($25.99 for 1.7 fl. oz.). Light moisture (without a heavy feel) make this niacinamide-laced drugstore option one to consider.
It�s time to call a truce in the war between oily skin and sunscreens! Bonus, each of the above six formulas will double as your daytime moisturizer (as in, they all include a nice array of anti-aging antioxidants and other beneficial ingredients), so you�ll have one less layer to use. For even more product recommendations for oily skin, be sure to check out the Best Products section of our sister site, Beautypedia!