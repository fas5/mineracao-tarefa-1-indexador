How Dermal Fillers Work

In This Article:

What Dermal Fillers are Used to Fix
How Dermal Fillers Work (and How They�re Different from Botox)
Synthetic and Natural Dermal Fillers
Risks from Dermal Fillers
Is there a Best Dermal Filler?
When to Get Dermal Fillers
Recommended Articles
If you�re reading this article, chances are that you at least have a rudimentary understanding of what dermal fillers are and what they do, but you may still have questions about how they work, how long they last, and when you should use them instead of (or with) Botox. We�ll answer all of those questions and more so you can decide if this cosmetic procedure is right for you!

What Dermal Fillers are Used to Fix

Dermal fillers can be used to fix a variety of concerns related to facial aging, including:

Adding volume and smoothing out the deep creases that run from the nose to the mouth (called nasolabial folds or laugh lines).
Plumping thin lips and smoothing out vertical lines around the edges of the lips.
Augmenting cheeks to enhance their shape, often restoring youthful fullness.
Filling out depressions (hollows) under the eye area.
Making indented scars from acne or chicken pox more even with surrounding skin.
Increasing volume to fill out gauntness in the lower cheek or temple area, which most often occurs due to fat pads shifting beneath the surface of skin.
How Dermal Fillers Work (and How They�re Different from Botox)

Dermal fillers are a naturally-derived or synthetic material that is directly injected into skin with the purpose of plumping that area to the point where the wrinkle, depression, or fold is gone. Depending on the type of filler, the effects can last anywhere from six months to two years; for semi-permanent or permanent fillers, the effects can last up to five years, and there are reports of even longer-lasting results [1].

Don't confuse dermal fillers with Botox. Although both procedures involve injections, Botox is most often injected around the forehead and wrinkles around the eyes (crow's feet) to stop muscle movement that results in wrinkles. It has nothing to do with the plumping smoothing effect dermal fillers have [2].

Because they work in different manners, many people elect to get both Botox and dermal fillers�the combination can produce a remarkably younger-looking face. [3]

Synthetic and Natural Dermal Fillers

There are two primary groups of dermal fillers: synthetic and natural.

Naturally-derived fillers (the most widely known is hyaluronic acid) have a much smaller risk of causing an allergic reaction but reactions can occur. Results are immediate but will only last from just three to 18 months (occasionally up to two years) because the filler eventually breaks down, taking the results, for better or worse, with it. Once the filler starts to dissipate you need to go back in and get more filler injected to maintain results. Most people who get fillers will need at least one follow-up injection within a year. [4, 5]

Aside from allergic reactions, fillers also have risks of lumping and migration, although this happens infrequently. On the other hand, because the results last a short period of time so do the potential problems. [6]

Synthetic fillers last much longer than naturally-derived ones and are considered semi-permanent though there many who would call them permanent because they really don't dissipate. The wrinkles start to return only because your face continues to age or you continue to get sun damage due to being lax about protecting skin with sunscreen daily. [7]

As with all fillers, synthetic fillers have risks which are pretty much the same as for naturally-derived fillers except the difference is because synthetic fillers are "permanent" the potential problems can be "permanent" as well and are harder to correct. [8]

Risks from Dermal Fillers

After all is said and done, the major issue for fillers is one of longevity, followed by risk of migration and lumping, and ultimately the skill of the doctor in placing the right amount of filler in exactly the right place. Regardless of the material, there is a learning curve to injection techniques as well as understanding how the varying substances affect skin. That means you need to find a doctor who has been injecting dermal fillers for some time, and who has loads of experience, preferably with more than one type of filler. Most people considering fillers have more than one area of concern fillers can address�and different fillers are available to specifically treat those concerns, such as lines around the mouth.

Is there a Best Dermal Filler?

Despite what you might have read or heard, there really isn't a best dermal filler; all of them have risks, albeit rare ones. Which filler substance is considered "best" or "preferred" depends on the doctor�s technique, skill, experience, training, your facial needs, and risk tolerance. It has nothing to do with headlines in the media or articles in fashion magazines.

It's important to realize that over the past 20 years many dermal fillers that were once showcased in fashion magazines or touted by doctors (and often heralded by studies paid for by the manufacturer) have since been discontinued for a variety of reasons. Getting headlines doesn't always make for beautiful results!

We wanted to at least take some of the guesswork out for you, so we compiled a Reviews of Dermal Fillers article to highlight the pros and cons of the most common and well researched options.

When to Get Dermal Fillers

There�s every reason to consider dermal fillers to correct signs of aging that are beyond what skincare products can do. It's not that great skincare can't make a huge difference in the appearance of your skin (believe us, we know the remarkable benefits a state-of-the-art skincare routine can provide) but age, muscle movement, fat loss, gravity, and sun damage among other factors will eventually take their toll. [9] Without question, fillers can help improve the appearance of deep lines and give skin a more supple, youthful appearance in ways that skincare simply cannot.

If you�re considering dermal fillers, the next step is to consult with your dermatologist to figure out which type of dermal filler is right for you.

And don�t forget, as with any cosmetic corrective procedure, you still need to use well-formulated skincare products and sun protection (even when it's cloudy) as part of the anti-aging package that will keep your skin looking younger and healthier for a very long time!