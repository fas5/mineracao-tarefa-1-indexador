Skin Irritation: Your Worst Enemy

We cannot stress this enough: irritation and inflammation are bad for skin�really, really bad. Daily assaults such as unprotected sun exposure, hot water, and applying skin-care products that contain irritating ingredients generate an irritant response. The result? Skin's repair process becomes faulty, collagen breaks down, and the skin's surface is weakened. For those with oily skin, irritation triggers nerve endings in the pore that activate hormones which increase oil production, leading to enlarged pores!

How Irritation Makes Oily Skin and Breakouts Worse

Inflammation in skin is usually related to external factors such as irritation that damages the skin�s barrier in numerous ways, whether you can see the reaction or not. When irritation on the surface of skin happens it activates specific chemicals called neuropeptides in the brain (Journal of Investigative Dermatology, 2007). Those substances are specifically the kind that regulates the hormonal system of the body.

When this happens, it then causes inflammatory chemicals targeted directly in the oil gland. Then, these inflammatory chemicals trigger an increase in oil production, which can increase the size of the pore, and the likelihood of acne�the more inflammation that occurs, the worse the risk (European Journal of Dermatology, 2002 and Dermatology, 2003).

Bottom line: Inflammation and its resulting irritation, whether internal or external (for this discussion externally it would be due to the use of irritating ingredients, hot water, overusing scrubs, etc.), is practically a guarantee you will see excess production of oil, larger pores and acne breakouts (Experimental Dermatology, 2009 and Dermato-Endocrinology, 2011).

That�s reason enough to avoid products with irritating ingredients, which often come in the form of fragrance including the misnamed �essential� oils.

Be Gentle!

For the overall health of your skin, anything you can do to treat it gently is a very good thing. Treating skin gently encourages normal collagen production, maintains a smooth and radiant surface, helps skin protect itself from environmental damage, reduces oil production, and makes pores smaller. We guarantee you will see potentially dramatic improvements to your skin simply by avoiding irritating products and learning to be gentle.

Avoid The Following...

- Overly abrasive scrubs (such as those that contain aluminum oxide crystals, walnut shells, or pumice)
- Astringents containing irritating ingredients (alcohol and menthol being the prime offenders)
- Toners containing irritating ingredients (alcohol and menthol being the prime offenders)
- Scrub mitts (think Buf-Puf)
- Cold or hot water
- Steaming or icing the skin
- Facial masks containing irritating ingredients (watch out for fragrant essential oils and polyvinyl alcohol)
- Loofahs
- Bar soaps and bar cleansers

Common Irritating Ingredients to Avoid

These ingredients are of greatest concern when they appear in the beginning of an ingredient list, or are among the first 10 ingredients. For more information on these ingredients and many more, consult the Cosmetic Ingredient Dictionary section of PaulasChoice.com.

- SD Alcohol (also known as denatured alcohol)
- Ammonia
- Arnica
- Balm mint
- Balsam
- Benzalkonium chloride
- Benzyl alcohol
- Bergamot
- Camphor
- Cinnamon
- Citrus juices and oils
- Clove
- Clover blossom
- Coriander
- Essential oils
- Ethanol
- Eucalyptus
- Eugenol
- Fennel
- Fennel oil
- Fir needle
- Fragrance
- Geranium
- Grapefruit
- Horsetail
- Isoeugenol
- Lavender
- Lemon
- Lemongrass
- Lime
- Linalool
- Marjoram
- Melissa (lemon balm)
- Menthol
- Menthoxypropanediol
- Menthyl Acetate
- Mint
- Oak bark
- Orange
- Papaya
- Peppermint
- Phenol
- Sandalwood oil
- Sodium C14-16 olefin sulfate
- Sodium lauryl sulfate
- TEA-lauryl sulfate
- Thyme
- Wintergreen
- Witch hazel
- Ylang-ylang